const Component = require("../model/Component");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Component
// @route     POST /api/v1/timeline/
// @access    Private
exports.addComponent = asyncHandler(async (req, res, next) => {
  const { componentName, data } = req.body;
  console.log(req.body);

  // Create admin
  const component = await Component.create({
    componentName,
    data,
  });

  res.status(200).json([
    component,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user data
// @route     PUT /api/v1/auth/updatedata
// @access    Private
exports.updateComponent = asyncHandler(async (req, res, next) => {
  const { componentName, data } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let component = await Component.findById(id);
  console.log(component);
  component = await Component.findByIdAndUpdate(
    id,
    { componentName, data },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(component);
});

exports.getComponent = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const component = await Component.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: component,
    });
  } else {
  }
});

exports.getComponents = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteComponent = asyncHandler(async (req, res, next) => {
  const component = await Component.findById(req.params.id);

  if (!component) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await component.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

const Home = require("../model/Home");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Home
// @route     POST /api/v1/timeline/
// @access    Private
exports.addHome = asyncHandler(async (req, res, next) => {
  const { icon, name, components } = req.body;
  console.log(req.body);

  // Create admin
  const home = await Home.create({
    icon,
    name,
    components,
  });

  res.status(200).json([
    home,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user components
// @route     PUT /api/v1/auth/updatecomponents
// @access    Private
exports.updateHome = asyncHandler(async (req, res, next) => {
  const { icon, name, components } = req.body;
  const id = req.params.id;
  console.log("home");
  console.log(id);
  // Update timeline
  let home = await Home.findById(id);
  console.log(home);
  home = await Home.findByIdAndUpdate(
    id,
    { icon, name, components },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(home);
});

exports.getHome = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const home = await Home.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: home,
    });
  } else {
  }
});

exports.getHomes = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteHome = asyncHandler(async (req, res, next) => {
  const home = await Home.findById(req.params.id);

  if (!home) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await home.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

const Type = require("../model/Type");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Type
// @route     POST /api/v1/timeline/
// @access    Private
exports.addType = asyncHandler(async (req, res, next) => {
  const { img_src, typeName, details, html, inBuild } = req.body;
  console.log(req.body);

  // Create admin
  const boundary = await Type.create({
    img_src,
    typeName,
    details,
    html,
    inBuild,
  });

  res.status(200).json([
    boundary,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details,html
// @route     PUT /api/v1/auth/updatedetails,html
// @access    Private
exports.updateType = asyncHandler(async (req, res, next) => {
  const { img_src, typeName, details, html, inBuild } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let boundary = await Type.findById(id);
  console.log(boundary);
  boundary = await Type.findByIdAndUpdate(
    id,
    { img_src, typeName, details, html, inBuild },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(boundary);
});

exports.getType = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const boundary = await Type.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: boundary,
    });
  } else {
  }
});

exports.getTypes = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteType = asyncHandler(async (req, res, next) => {
  const boundary = await Type.findById(req.params.id);

  if (!boundary) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await boundary.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

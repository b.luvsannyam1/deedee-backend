const FilterGroup = require("../model/FilterGroup");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new FilterGroup
// @route     POST /api/v1/timeline/
// @access    Private
exports.addFilterGroup = asyncHandler(async (req, res, next) => {
  const { icon, name, FilterGroupGroup } = req.body;
  console.log(req.body);

  // Create admin
  const FilterGroup = await FilterGroup.create({
    icon,
    name,
    FilterGroupGroup,
  });

  res.status(200).json([
    FilterGroup,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user
// @route     PUT /api/v1/auth/update
// @access    Private
exports.updateFilterGroup = asyncHandler(async (req, res, next) => {
  const { icon, name, FilterGroupGroup } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let FilterGroup = await FilterGroup.findById(id);
  console.log(FilterGroup);
  FilterGroup = await FilterGroup.findByIdAndUpdate(
    id,
    { icon, name, FilterGroupGroup },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(FilterGroup);
});

exports.getFilterGroup = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const FilterGroup = await FilterGroup.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: FilterGroup,
    });
  } else {
  }
});

exports.getFilterGroups = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteFilterGroup = asyncHandler(async (req, res, next) => {
  const FilterGroup = await FilterGroup.findById(req.params.id);

  if (!FilterGroup) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await FilterGroup.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

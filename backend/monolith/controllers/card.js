const Card = require("../model/Card");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Card
// @route     POST /api/v1/timeline/
// @access    Private
exports.addCard = asyncHandler(async (req, res, next) => {
  const { imageUrl, cardName, description } = req.body;
  console.log(req.body);

  // Create admin
  const brand = await Card.create({
    imageUrl,
    cardName,
    description,
  });

  res.status(200).json([
    brand,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user description
// @route     PUT /api/v1/auth/updatedescription
// @access    Private
exports.updateCard = asyncHandler(async (req, res, next) => {
  const { imageUrl, cardName, description } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let brand = await Card.findById(id);
  console.log(brand);
  brand = await Card.findByIdAndUpdate(
    id,
    { imageUrl, cardName, description },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(brand);
});

exports.getCard = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const brand = await Card.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: brand,
    });
  } else {
  }
});

exports.getCards = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteCard = asyncHandler(async (req, res, next) => {
  const brand = await Card.findById(req.params.id);

  if (!brand) {
    return next(
      new ErrorResponse(`No card with the id of ${req.params.id}`),
      404
    );
  }
  await brand.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

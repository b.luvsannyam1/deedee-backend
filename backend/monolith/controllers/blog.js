const Blog = require("../model/Blog");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Blog
// @route     POST /api/v1/timeline/
// @access    Private
exports.addBlog = asyncHandler(async (req, res, next) => {
  const { banner_url, title, description, content } = req.body;
  console.log(req.body);

  // Create admin
  const blog = await Blog.create({
    banner_url,
    title,
    description,
    content,
  });

  res.status(200).json([
    blog,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateBlog = asyncHandler(async (req, res, next) => {
  const { banner_url, title, description, content } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let blog = await Blog.findById(id);
  console.log(blog);
  blog = await Blog.findByIdAndUpdate(
    id,
    { banner_url, title, description, content },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(blog);
});

exports.getBlog = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const blog = await Blog.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: blog,
    });
  } else {
  }
});

exports.getBlogs = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteBlog = asyncHandler(async (req, res, next) => {
  const blog = await Blog.findById(req.params.id);

  if (!blog) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await blog.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

const express = require("express");
const {
  addCard,
  updateCard,
  getCard,
  getCards,
  deleteCard,
} = require("../controller/card");
const Card = require("../model/Card");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");

const { protect } = require("../middleware/auth");
router.route("/").get(advancedResults(Card), getCards).post(addCard);
router.route("/:id").get(getCard).put(updateCard).delete(deleteCard);

module.exports = router;

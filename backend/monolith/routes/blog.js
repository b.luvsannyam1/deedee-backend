const express = require("express");
const {
  addBlog,
  updateBlog,
  getBlog,
  getBlogs,
  deleteBlog,
} = require("../controller/blog");
const Blog = require("../model/Blog");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");

const { protect } = require("../middleware/auth");
router.route("/").get(advancedResults(Blog), getBlogs).post(addBlog);
router.route("/:id").get(getBlog).put(updateBlog).delete(deleteBlog);

module.exports = router;

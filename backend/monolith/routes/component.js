const express = require("express");
const {
  addComponent,
  updateComponent,
  getComponent,
  getComponents,
  deleteComponent,
} = require("../controller/component");
const Component = require("../model/Component");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");

const { protect } = require("../middleware/auth");
router
  .route("/")
  .get(
    advancedResults(Component, ["data.products", "data.card"]),
    getComponents
  )
  .post(addComponent);
router
  .route("/:id")
  .get(getComponent)
  .put(updateComponent)
  .delete(deleteComponent);

module.exports = router;

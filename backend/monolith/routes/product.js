const express = require("express");
const {
  addProduct,
  updateProduct,
  getProduct,
  getProducts,
  deleteProduct,
  getHelp,
} = require("../controller/product");
const Product = require("../model/Product");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");
const advancedProducts = require("../middleware/advencedProducts");
const { protect } = require("../middleware/auth");
router
  .route("/")
  .get(
    advancedProducts(["brand", "manufacturer", "type", "filters", "boundary"]),
    getProducts
  )
  .post(addProduct);
router.route("/:id").get(getProduct).put(updateProduct).delete(deleteProduct);
router.route("/help".get(getHelp));
module.exports = router;

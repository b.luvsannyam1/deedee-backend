const express = require("express");
const {
  addHome,
  updateHome,
  getHome,
  getHomes,
  deleteHome,
} = require("../controller/home");
const Home = require("../model/Home");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");

const { protect } = require("../middleware/auth");
router
  .route("/")
  .get(
    advancedResults(Home, {
      path: "components",
      populate: ["data.products", "data.card"],
    }),
    getHomes
  )
  .post(addHome);
router.route("/:id").get(getHome).put(updateHome).delete(deleteHome);

module.exports = router;

const express = require("express");
const {
  addType,
  updateType,
  getType,
  getTypes,
  deleteType,
  ImageHandler,
} = require("../controller/type");
const Type = require("../model/Type");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");

const { protect } = require("../middleware/auth");
router.route("/").get(advancedResults(Type), getTypes).post(addType);
router.route("/:id").get(getType).put(updateType).delete(deleteType);
router.route("/image").post(ImageHandler);

module.exports = router;

//Хэрэгцээтэй либрари бүрийг дуудаж байна
const path = require("path");
const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan");
const fileupload = require("express-fileupload");
const cookieParser = require("cookie-parser");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const rateLimit = require("express-rate-limit");
const hpp = require("hpp");
const cors = require("cors");
const connectDB = require("./config/db");
const fileUpload = require("express-fileupload");

var request = require("request");
// Environment файлын байршил
dotenv.config({ path: "./config/config.env" });

const auth = require("./route/user-based/auth");

//product-based routes
const boundary = require("./route/product-based/boundary");
const product = require("./route/product-based/product");
const brand = require("./route/product-based/brand");
const filter = require("./route/product-based/filter");
const manufacturer = require("./route/product-based/manufacturer");
const productcategory = require("./route/product-based/productcategory");
const type = require("./route/product-based/type");
const navbar = require("./route/web-based/navbar");

//vending-based routes
const model = require("./route/vending-based/model");
// const vendingmachine = require("./route/vending-based/vendingmachine");

//web-based routes
const blog = require("./route/web-based/blog");
const card = require("./route/web-based/ratiocard");
const constant = require("./route/web-based/constant");
const mailer = require("./route/web-based/mailer");
const { getMaxListeners } = require("./model/user-based/User");

// Connect to database
connectDB();
//app-ыг Express гэдгийг зарлаж өгнө
const app = express();
// Body parser
app.use(express.json());

// Cookie parser
app.use(cookieParser());

// Dev logging middleware
app.use(morgan("dev"));

// File uploading
// app.use(fileupload());

// Sanitize data
app.use(mongoSanitize());

// Prevent XSS attacks
// app.use(xss());

// Rate limiting
const limiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 10 mins
  max: 1000000,
});
app.use(limiter);

// Prevent http param pollution
app.use(hpp());

//Cors in app
const corsOptions = {
  credentials: true,
  origin: "http://localhost:3000",
};

app.use(cors(corsOptions));
app.use(
  fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
  })
);
app.use(
  "/dashboard",
  express.static(path.join(__dirname, "public/admin/build"))
);
app.use("/", express.static(path.join(__dirname, "public/build")));

app.use("/uploads", express.static(path.join(__dirname, "public/images")));
app.use("/images", express.static(path.join(__dirname, "public/build/images")));

//user-based route implementation
app.use("/server/v1/admin/auth", auth);

//product-based route implementation
app.use("/server/v1/admin/boundary", boundary);
app.use("/server/v1/admin/product", product);
app.use("/server/v1/admin/brand", brand);
app.use("/server/v1/admin/filter", filter);
app.use("/server/v1/admin/manufacturer", manufacturer);
app.use("/server/v1/admin/productcategory", productcategory);
app.use("/server/v1/admin/type", type);

//web-based route implementation
app.use("/server/v1/admin/blog", blog);
app.use("/server/v1/admin/card", card);
app.use("/server/v1/admin/constant", constant);
app.use("/server/v1/admin/navbar", navbar);
app.use("/server/v1/admin/mailer", mailer);

//vending-based route implementation
app.use("/server/v1/admin/type", type);

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "public", "build", "index.html"));
});
app.get("/dashboard", (req, res) => {
  res.sendFile(
    path.resolve(__dirname, "public", "admin", "build", "index.html")
  );
});
const PORT = 5000;

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});
// Handle unhandled promise rejections
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}`);
  // Close server & exit process
  app.close(() => process.exit(1));
});

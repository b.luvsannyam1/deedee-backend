const ProjectFilters = require("../../model/order-based/ProjectFilters");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new ProjectFilters
// @route     POST /api/v1/timeline/
// @access    Private
exports.addProjectFilters = asyncHandler(async (req, res, next) => {
  const { handle, title, icon } = req.body;

  const projectfilter = await ProjectFilters.create({
    handle,
    title,
    icon,
  });

  res.status(200).json([
    projectfilter,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getProjectFilters = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const projectfilter = await ProjectFilters.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: projectfilter,
    });
  } else {
  }
});

exports.getProjectFilterses = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteProjectFilters = asyncHandler(async (req, res, next) => {
  const projectfilter = await ProjectFilters.findById(req.params.id);

  if (!projectfilter) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await projectfilter.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

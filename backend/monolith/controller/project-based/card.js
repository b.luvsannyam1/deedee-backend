const Card = require("../../model/order-based/Card");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Card
// @route     POST /api/v1/timeline/
// @access    Private
exports.addCard = asyncHandler(async (req, res, next) => {
  const {
    name,
    description,
    idAttachmentCover,
    idMembers,
    idLabels,
    attachments,
    subscribed,
    checklist,
    activities,
  } = req.body;

  const card = await Card.create({
    name,
    description,
    idAttachmentCover,
    idMembers,
    idLabels,
    attachments,
    subscribed,
    checklist,
    activities,
  });

  res.status(200).json([
    card,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getCard = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const card = await Card.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: card,
    });
  } else {
  }
});

exports.getCardes = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteCard = asyncHandler(async (req, res, next) => {
  const card = await Card.findById(req.params.id);

  if (!card) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await card.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

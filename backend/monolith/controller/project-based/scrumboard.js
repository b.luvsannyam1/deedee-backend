const ProjectLabel = require("../../model/order-based/ProjectLabel");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new ProjectLabel
// @route     POST /api/v1/timeline/
// @access    Private
exports.addProjectLabel = asyncHandler(async (req, res, next) => {
  const { handle, title, color } = req.body;

  const projectlabel = await ProjectLabel.create({
    handle,
    title,
    color,
  });

  res.status(200).json([
    projectlabel,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getProjectLabel = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const projectlabel = await ProjectLabel.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: projectlabel,
    });
  } else {
  }
});

exports.getProjectLabels = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteProjectLabel = asyncHandler(async (req, res, next) => {
  const projectlabel = await ProjectLabel.findById(req.params.id);

  if (!projectlabel) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await projectlabel.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

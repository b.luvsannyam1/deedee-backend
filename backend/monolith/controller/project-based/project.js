const Project = require("../../model/order-based/Project");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Project
// @route     POST /api/v1/timeline/
// @access    Private
exports.addProject = asyncHandler(async (req, res, next) => {
  const { name } = req.body;

  const project = await Project.create({
    name,
  });

  res.status(200).json([
    project,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getProject = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const project = await Project.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: project,
    });
  } else {
  }
});

exports.getProjectes = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteProject = asyncHandler(async (req, res, next) => {
  const project = await Project.findById(req.params.id);

  if (!project) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await project.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

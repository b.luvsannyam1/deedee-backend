const NavBar = require("../../model/web-based/NavBar");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

// @desc      Add a new NavBar
// @route     POST /api/v1/timeline/
// @access    Private
exports.addNavBar = asyncHandler(async (req, res, next) => {
  const { toplinks, bottomLinks } = req.body;

  const model = await NavBar.create({
    toplinks,
    bottomLinks,
  });

  res.status(200).json([
    model,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateNavBar = asyncHandler(async (req, res, next) => {
  const { toplinks, bottomLinks } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let model = await NavBar.findById(id);
  brand = await NavBar.findByIdAndUpdate(
    id,
    { toplinks, bottomLinks },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(brand);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getNavBar = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const model = await NavBar.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: model,
    });
  } else {
    return res.status(500).json({
      success: false,
      data: model,
    });
  }
});

exports.getNavBars = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteNavBar = asyncHandler(async (req, res, next) => {
  const model = await NavBar.findById(req.params.id);

  if (!model) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await model.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

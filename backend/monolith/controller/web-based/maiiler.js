const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const SendEmail = require("../../utils/sendEmail");

// @desc      Add a new Blog
// @route     POST /api/v1/blog/
// @access    Private
exports.sendEmail = asyncHandler(async (req, res, next) => {
  const { address, pnumber, products } = req.body;

  SendEmail({ address, pnumber, products });

  res.status(200).json([
    {
      new: true,
      runValidators: true,
    },
  ]);
});

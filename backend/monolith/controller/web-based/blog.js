const Blog = require("../../model/web-based/Blog");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");

const fs = require("fs");

// @desc      Add a new Blog
// @route     POST /api/v1/blog/
// @access    Private
exports.addBlog = asyncHandler(async (req, res, next) => {
  const { title, banner, description, featured, content } = req.body;

  const blog = await Blog.create({
    title,
    banner,
    description,
    featured,
    content,
  });

  res.status(200).json([
    blog,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/blog/update
// @access    Private
exports.updateBlog = asyncHandler(async (req, res, next) => {
  const { title, banner, description, featured, content } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let blog = await Blog.findById(id);
  if (blog) {
    blog = await Blog.findByIdAndUpdate(
      id,
      { title, banner, description, featured, content },
      {
        new: true,
        runValidators: true,
      }
    );
  }

  res.status(200).json(blog);
});

// @desc      Get user details
// @route     PUT /api/v1/blog/:id
// @access    Private

exports.getBlog = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const blog = await Blog.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: blog,
    });
  } else {
  }
});

exports.getBlogs = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteBlog = asyncHandler(async (req, res, next) => {
  const blog = await Blog.findById(req.params.id);

  if (!blog) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await blog.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

exports.ImageHandler = asyncHandler(async (req, res, next) => {
  if (req.files.upload) {
    const imagePath =
      __dirname + "/../../public/images/blog/" + req.files.upload.name;
    fs.writeFile(imagePath, req.files.upload.data, function (err) {
      if (err) return console.log(err);
    });
    const linkToImage =
      "/uploads/blog/" + req.files.upload.name;
    return res.status(200).json({
      success: true,
      url: linkToImage,
      default: linkToImage,
    });
  } else {
    return res.status(500).json({
      success: false,
      data: null,
    });
  }
});

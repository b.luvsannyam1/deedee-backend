const Constant = require("../../model/web-based/Constant");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

// @desc      Add a new Constant
// @route     POST /api/v1/timeline/
// @access    Private
exports.addConstant = asyncHandler(async (req, res, next) => {
  const { name, value, description } = req.body;

  const model = await Constant.create({
    name,
    value,
    description,
  });

  res.status(200).json([
    model,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateConstant = asyncHandler(async (req, res, next) => {
  const { name, value, description } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let model = await Constant.findById(id);
  brand = await Constant.findByIdAndUpdate(
    id,
    { name, value, description },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(brand);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getConstant = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const model = await Constant.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: model,
    });
  } else {
    return res.status(500).json({
      success: false,
      data: model,
    });
  }
});

exports.getConstants = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteConstant = asyncHandler(async (req, res, next) => {
  const model = await Constant.findById(req.params.id);

  if (!model) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await model.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

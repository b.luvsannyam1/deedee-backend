const RatioCard = require("../../model/web-based/RatioCard");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

const fs = require("fs");
// @desc      Add a new RatioCard
// @route     POST /api/v1/ratiocard/
// @access    Private
exports.addRatioCard = asyncHandler(async (req, res, next) => {
  const { name, description, cardType, position, img_src, linkTo } = req.body;

  const ratiocard = await RatioCard.create({
    name,
    description,
    cardType,
    position,
    img_src,
    linkTo,
  });

  res.status(200).json([
    ratiocard,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/ratiocard/update
// @access    Private
exports.updateRatioCard = asyncHandler(async (req, res, next) => {
  const { name, description, cardType, position, img_src, linkTo } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let ratiocard = await RatioCard.findById(id);
  if (ratiocard) {
    ratiocard = await RatioCard.findByIdAndUpdate(
      id,
      { name, description, cardType, position, img_src, linkTo },
      {
        new: true,
        runValidators: true,
      }
    );
  }

  res.status(200).json(ratiocard);
});

// @desc      Get user details
// @route     PUT /api/v1/ratiocard/:id
// @access    Private

exports.getRatioCard = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const ratiocard = await RatioCard.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: ratiocard,
    });
  } else {
  }
});

exports.getRatioCards = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteRatioCard = asyncHandler(async (req, res, next) => {
  const ratiocard = await RatioCard.findById(req.params.id);

  if (!ratiocard) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await ratiocard.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});
exports.ImageHandler = asyncHandler(async (req, res, next) => {
  if (req.files.upload) {
    const imagePath =
      __dirname + "/../../public/images/card/" + req.files.upload.name;
    fs.writeFile(imagePath, req.files.upload.data, function (err) {
      if (err) return console.log(err);
    });
    const linkToImage = "/uploads/card/" + req.files.upload.name;
    return res.status(200).json({
      success: true,
      url: linkToImage,
      default: linkToImage,
    });
  } else {
    return res.status(500).json({
      success: false,
      data: null,
    });
  }
});

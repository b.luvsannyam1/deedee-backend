const OrderStatus = require("../../model/order-based/OrderStatus");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new OrderStatus
// @route     POST /api/v1/timeline/
// @access    Private
exports.addOrderStatus = asyncHandler(async (req, res, next) => {
  const { type } = req.body;

  const orderStatus = await OrderStatus.create({
    type,
  });

  res.status(200).json([
    orderStatus,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getOrderStatus = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const orderStatus = await OrderStatus.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: orderStatus,
    });
  } else {
  }
});

exports.getOrderStatuses = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteOrderStatus = asyncHandler(async (req, res, next) => {
  const orderStatus = await OrderStatus.findById(req.params.id);

  if (!orderStatus) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await orderStatus.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

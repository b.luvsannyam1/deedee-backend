const Order = require("../../model/order-based/Order");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Order
// @route     POST /api/v1/timeline/
// @access    Private
exports.addOrder = asyncHandler(async (req, res, next) => {
  const {
    subtotal,
    total,
    tax,
    discount,
    vendingMachine,
    products,
    status,
    payment,
  } = req.body;

  const order = await Order.create({
    subtotal,
    total,
    tax,
    discount,
    vendingMachine,
    products,
    status,
    payment,
  });

  res.status(200).json([
    order,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getOrder = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const order = await Order.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: order,
    });
  } else {
  }
});

exports.getOrders = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteOrder = asyncHandler(async (req, res, next) => {
  const order = await Order.findById(req.params.id);

  if (!order) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await order.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

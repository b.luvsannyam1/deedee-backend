const TransactionMethod = require("../../model/order-based/TransactionMethod");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new TransactionMethod
// @route     POST /api/v1/timeline/
// @access    Private
exports.addTransactionMethod = asyncHandler(async (req, res, next) => {
  const { name, supplier } = req.body;

  const transactionMethod = await TransactionMethod.create({
    name,
    supplier,
  });

  res.status(200).json([
    transactionMethod,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getTransactionMethod = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const transactionMethod = await TransactionMethod.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: transactionMethod,
    });
  } else {
  }
});

exports.getTransactionMethodes = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteTransactionMethod = asyncHandler(async (req, res, next) => {
  const transactionMethod = await TransactionMethod.findById(req.params.id);

  if (!transactionMethod) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await transactionMethod.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

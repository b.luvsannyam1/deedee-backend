const Transaction = require("../../model/order-based/Transaction");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Transaction
// @route     POST /api/v1/timeline/
// @access    Private
exports.addTransaction = asyncHandler(async (req, res, next) => {
  const { type } = req.body;

  const transaction = await Transaction.create({
    type,
  });

  res.status(200).json([
    transaction,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getTransaction = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const transaction = await Transaction.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: transaction,
    });
  } else {
  }
});

exports.getTransactiones = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteTransaction = asyncHandler(async (req, res, next) => {
  const transaction = await Transaction.findById(req.params.id);

  if (!transaction) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await transaction.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

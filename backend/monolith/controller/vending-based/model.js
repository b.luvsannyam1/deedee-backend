const Model = require("../../model/vending-based/Model");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

// @desc      Add a new Model
// @route     POST /api/v1/timeline/
// @access    Private
exports.addModel = asyncHandler(async (req, res, next) => {
  const { name, description } = req.body;

  const model = await Model.create({
    name,
    description,
  });

  res.status(200).json([
    model,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateModel = asyncHandler(async (req, res, next) => {
  const { icon, brandName, details } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let model = await Model.findById(id);
  brand = await Model.findByIdAndUpdate(
    id,
    { icon, brandName, details },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(brand);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getModel = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const model = await Model.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: model,
    });
  } else {
  }
});

exports.getModeles = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteModel = asyncHandler(async (req, res, next) => {
  const model = await Model.findById(req.params.id);

  if (!model) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await model.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

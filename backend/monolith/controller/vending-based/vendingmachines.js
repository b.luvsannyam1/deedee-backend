const VendingMachine = require("../../model/vending-based/VendingMachine");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new VendingMachine
// @route     POST /api/v1/timeline/
// @access    Private
exports.addVendingMachine = asyncHandler(async (req, res, next) => {
  const { name, model, inventory, maximumCapacity, ownedBy, supposedAddress } =
    req.body;

  const model = await VendingMachine.create({
    name,
    description,
  });

  res.status(200).json([
    model,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateVendingMachine = asyncHandler(async (req, res, next) => {
  const { name, model, inventory, maximumCapacity, ownedBy, supposedAddress } =
    req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let model = await VendingMachine.findById(id);
  brand = await VendingMachine.findByIdAndUpdate(
    id,
    { icon, brandName, details },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(brand);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getVendingMachine = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const model = await VendingMachine.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: model,
    });
  } else {
  }
});

exports.getVendingMachinees = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteVendingMachine = asyncHandler(async (req, res, next) => {
  const model = await VendingMachine.findById(req.params.id);

  if (!model) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await model.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

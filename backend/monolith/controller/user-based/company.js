const Company = require("../../model/user-based/Company");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const path = require("path");

// @desc      Add a new Company
// @route     POST /api/v1/timeline/
// @access    Private
exports.addCompany = asyncHandler(async (req, res, next) => {
  const { displayName, description, ownedMachines } = req.body;

  const company = await Company.create({
    displayName,
    description,
    ownedMachines,
  });

  res.status(200).json([
    company,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateCompany = asyncHandler(async (req, res, next) => {
  const { icon, brandName, details } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let company = await Company.findById(id);
  brand = await Company.findByIdAndUpdate(
    id,
    { icon, brandName, details },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(brand);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private

exports.getCompany = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const company = await Company.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: company,
    });
  } else {
  }
});

exports.getCompanyes = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteCompany = asyncHandler(async (req, res, next) => {
  const company = await Company.findById(req.params.id);

  if (!company) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await company.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

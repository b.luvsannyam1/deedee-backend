const ProductCategory = require("../../model/product-based/ProductCategory");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

// @desc      Add a new ProductCategory
// @route     POST /api/v1/timeline/
// @access    Private
exports.addProductCategory = asyncHandler(async (req, res, next) => {
  const { name } = req.body;
  console.log(req.body);

  // Create admin
  const boundary = await ProductCategory.create({
    name,
  });

  res.status(200).json([
    boundary,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateProductCategory = asyncHandler(async (req, res, next) => {
  const { name } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let boundary = await ProductCategory.findById(id);
  console.log(boundary);
  boundary = await ProductCategory.findByIdAndUpdate(
    id,
    { name },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(boundary);
});

exports.getProductCategory = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const boundary = await ProductCategory.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: boundary,
    });
  } else {
  }
});

exports.getProductCategorys = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteProductCategory = asyncHandler(async (req, res, next) => {
  const boundary = await ProductCategory.findById(req.params.id);

  if (!boundary) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await boundary.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

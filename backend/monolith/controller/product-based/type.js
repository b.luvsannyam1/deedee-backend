const Type = require("../../model/product-based/Type");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

// @desc      Add a new Type
// @route     POST /api/v1/timeline/
// @access    Private
exports.addType = asyncHandler(async (req, res, next) => {
  const { typeName, details } = req.body;
  console.log(req.body);

  // Create admin
  const type = await Type.create({
    typeName,
    details,
  });

  res.status(200).json([
    type,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details,html
// @route     PUT /api/v1/auth/updatedetails,html
// @access    Private
exports.updateType = asyncHandler(async (req, res, next) => {
  const { typeName, details } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let type = await Type.findById(id);
  console.log(type);
  type = await Type.findByIdAndUpdate(
    id,
    { typeName, details },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(type);
});

exports.getType = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const type = await Type.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: type,
    });
  } else {
  }
});

exports.getTypes = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteType = asyncHandler(async (req, res, next) => {
  const type = await Type.findById(req.params.id);

  if (!type) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await type.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

exports.ImageHandler = asyncHandler(async (req, res, next) => {
  if (req.files.upload) {
    const imagePath =
      __dirname + "/../../public/images/type/" + req.files.upload.name;
    fs.writeFile(imagePath, req.files.upload.data, function (err) {
      if (err) return console.log(err);
    });
    const linkToImage =
      "http://localhost:5000/uploads/type/" + req.files.upload.name;
    return res.status(200).json({
      success: true,
      url: linkToImage,
      default: linkToImage,
    });
  } else {
    return res.status(500).json({
      success: false,
      data: null,
    });
  }
});

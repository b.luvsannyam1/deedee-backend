const Filter = require("../../model/product-based/Filter");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

// @desc      Add a new Filter
// @route     POST /api/v1/timeline/
// @access    Private
exports.addFilter = asyncHandler(async (req, res, next) => {
  const { name, description } = req.body;
  console.log(req.body);

  // Create admin
  const filter = await Filter.create({
    name,
    description,
  });

  res.status(200).json([
    filter,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user
// @route     PUT /api/v1/auth/update
// @access    Private
exports.updateFilter = asyncHandler(async (req, res, next) => {
  const { name, description } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let filter = await Filter.findById(id);
  console.log(filter);
  filter = await Filter.findByIdAndUpdate(
    id,
    { name, description },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(filter);
});

exports.getFilter = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const filter = await Filter.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: filter,
    });
  } else {
  }
});

exports.getFilters = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteFilter = asyncHandler(async (req, res, next) => {
  const filter = await Filter.findById(req.params.id);

  if (!filter) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await filter.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

exports.ImageHandler = asyncHandler(async (req, res, next) => {
  const imagePath =
    __dirname + "/../../public/images/filter/" + req.files.image.name;
  fs.writeFile(imagePath, req.files.image.data, function (err) {
    if (err) return console.log(err);
  });
  return res.status(200).json({
    success: true,
    data: imagePath,
  });
});

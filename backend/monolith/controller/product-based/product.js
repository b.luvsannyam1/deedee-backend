const Product = require("../../model/product-based/Product");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");
const Boundary = require("../../model/product-based/Boundary");
const Brand = require("../../model/product-based/Brand");
const Filter = require("../../model/product-based/Filter");
const Manufacturer = require("../../model/product-based/Manufacturer");
const ProductCategory = require("../../model/product-based/ProductCategory");
const Type = require("../../model/product-based/Type");
const fs = require("fs");
// @desc      Add a new Product
// @route     POST /api/v1/timeline/
// @access    Private
exports.addProduct = asyncHandler(async (req, res, next) => {
  const {
    name,
    brand,
    manufacturer,
    type,
    filters,
    boundary,
    description,
    featureOn,
    html,
    price,
    featuredImageId,
    images,
    weight,
    activeForUsers,
  } = req.body;
  console.log(req.body);

  // Create admin
  const product = await Product.create({
    name,
    brand,
    manufacturer,
    type,
    filters,
    boundary,
    description,
    featureOn,
    html,
    featuredImageId,
    price,
    images,
    weight,
    activeForUsers,
  });

  res.status(200).json([
    product,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user html,spec,powerConsumption,price,brand,manufacturer,type,filters,boundary,brand
// @route     PUT /api/v1/auth/updatehtml,spec,powerConsumption,price,brand,manufacturer,type,filters,boundary,brand
// @access    Private
exports.updateProduct = asyncHandler(async (req, res, next) => {
  const {
    name,
    brand,
    manufacturer,
    type,
    filters,
    boundary,
    description,
    featureOn,
    html,
    featuredImageId,
    price,
    images,
    weight,
    activeForUsers,
  } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let product = await Product.findById(id);
  console.log(boundary);
  product = await Product.findByIdAndUpdate(
    id,
    {
      name,
      brand,
      manufacturer,
      type,
      filters,
      boundary,
      description,
      featureOn,
      html,
      price,
      featuredImageId,
      images,
      weight,
      activeForUsers,
    },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(boundary);
});

exports.getProduct = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const product = await Product.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: product,
    });
  } else {
  }
});

exports.getProducts = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});
exports.getHelp = asyncHandler(async (req, res, next) => {
  const boundary = await Boundary.find();
  const brand = await Brand.find();
  const filter = await Filter.find();
  const manufacturer = await Manufacturer.find();
  const category = await ProductCategory.find();
  const type = await Type.find();
  res
    .status(200)
    .json({ boundary, brand, filter, manufacturer, category, type });
});
exports.deleteProduct = asyncHandler(async (req, res, next) => {
  const product = await Product.findById(req.params.id);

  if (!product) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await product.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

exports.ImageHandler = asyncHandler(async (req, res, next) => {
  if (req.files.upload) {
    const imagePath =
      __dirname + "/../../public/images/product/" + req.files.upload.name;
    fs.writeFile(imagePath, req.files.upload.data, function (err) {
      if (err) return console.log(err);
    });
    const linkToImage = "/uploads/product/" + req.files.upload.name;
    return res.status(200).json({
      success: true,
      url: linkToImage,
      default: linkToImage,
    });
  } else {
    return res.status(500).json({
      success: false,
      data: null,
    });
  }
});

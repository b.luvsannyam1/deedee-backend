const Brand = require("../../model/product-based/Brand");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

const fs = require("fs");
// @desc      Add a new Brand
// @route     POST /api/v1/timeline/
// @access    Private
exports.addBrand = asyncHandler(async (req, res, next) => {
  const { brandName, details } = req.body;
  console.log(req.body);

  // Create admin
  const brand = await Brand.create({
    brandName,
    details,
  });

  res.status(200).json([
    brand,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateBrand = asyncHandler(async (req, res, next) => {
  const { brandName, details } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let brand = await Brand.findById(id);
  console.log(brand);
  brand = await Brand.findByIdAndUpdate(
    id,
    { brandName, details },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(brand);
});

exports.getBrand = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const brand = await Brand.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: { brand },
    });
  } else {
  }
});

exports.getBrands = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteBrand = asyncHandler(async (req, res, next) => {
  const brand = await Brand.findById(req.params.id);

  if (!brand) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await brand.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});
exports.ImageHandler = asyncHandler(async (req, res, next) => {
  const imagePath =
    __dirname + "/../../public/images/brand/" + req.files.image.name;
  fs.writeFile(imagePath, req.files.image.data, function (err) {
    if (err) return console.log(err);
  });
  return res.status(200).json({
    success: true,
    data: imagePath,
  });
});

const Boundary = require("../../model/product-based/Boundary");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

const fs = require("fs");
// @desc      Add a new Boundary
// @route     POST /api/v1/timeline/
// @access    Private
exports.addBoundary = asyncHandler(async (req, res, next) => {
  const { icon, boundaryName, details } = req.body;
  console.log(req.body);

  // Create admin
  const boundary = await Boundary.create({
    icon,
    boundaryName,
    details,
  });

  res.status(200).json([
    boundary,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateBoundary = asyncHandler(async (req, res, next) => {
  const { icon, boundaryName, details } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let boundary = await Boundary.findById(id);
  console.log(boundary);
  boundary = await Boundary.findByIdAndUpdate(
    id,
    { icon, boundaryName, details },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(boundary);
});

exports.getBoundary = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const boundary = await Boundary.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: boundary,
    });
  } else {
  }
});

exports.getBoundarys = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteBoundary = asyncHandler(async (req, res, next) => {
  const boundary = await Boundary.findById(req.params.id);

  if (!boundary) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await boundary.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

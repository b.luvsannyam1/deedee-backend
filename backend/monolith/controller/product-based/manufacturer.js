const Manufacturer = require("../../model/product-based/Manufacturer");
const ErrorResponse = require("../../utils/errorResponse");
const asyncHandler = require("../../middleware/async");
const path = require("path");

const fs = require("fs");
// @desc      Add a new Manufacturer
// @route     POST /api/v1/timeline/
// @access    Private
exports.addManufacturer = asyncHandler(async (req, res, next) => {
  const { manufacturerName, details } = req.body;
  console.log(req.body);

  // Create admin
  const boundary = await Manufacturer.create({
    manufacturerName,
    details,
  });

  res.status(200).json([
    boundary,
    {
      new: true,
      runValidators: true,
    },
  ]);
});

// @desc      Update user details
// @route     PUT /api/v1/auth/updatedetails
// @access    Private
exports.updateManufacturer = asyncHandler(async (req, res, next) => {
  const { manufacturerName, details } = req.body;
  const id = req.params.id;
  console.log(id);
  // Update timeline
  let boundary = await Manufacturer.findById(id);
  console.log(boundary);
  boundary = await Manufacturer.findByIdAndUpdate(
    id,
    { manufacturerName, details },
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(200).json(boundary);
});

exports.getManufacturer = asyncHandler(async (req, res, next) => {
  console.log(res);
  if (req.params.id) {
    const boundary = await Manufacturer.findById(req.params.id);

    return res.status(200).json({
      success: true,
      data: boundary,
    });
  } else {
  }
});

exports.getManufacturers = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.deleteManufacturer = asyncHandler(async (req, res, next) => {
  const boundary = await Manufacturer.findById(req.params.id);

  if (!boundary) {
    return next(
      new ErrorResponse(`No timeline with the id of ${req.params.id}`),
      404
    );
  }
  await boundary.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

exports.ImageHandler = asyncHandler(async (req, res, next) => {
  if (req.files.upload) {
    const imagePath =
      __dirname + "/../../public/images/manufacturer/" + req.files.upload.name;
    fs.writeFile(imagePath, req.files.upload.data, function (err) {
      if (err) return console.log(err);
    });
    const linkToImage =
      "/uploads/manufacturer/" + req.files.upload.name;
    return res.status(200).json({
      success: true,
      url: linkToImage,
      default: linkToImage,
    });
  } else {
    return res.status(500).json({
      success: false,
      data: null,
    });
  }
});

const express = require("express");
const {
  uploadImage,
  addAbout,
  updateAbout,
  getAbout,
  getAbouts,
  deleteAbout,
} = require("../controllers/About");
const About = require("../models/About");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");

const { protect } = require("../middleware/auth");
router.route("/").get(advancedResults(About), getAbouts).post(addAbout);
router.route("/:id").get(getAbout).put(updateAbout).delete(deleteAbout);
router.post("/image", uploadImage);
// router.post("/updateContent/:id", updateContent);

// router.get("/:id", getContent);

module.exports = router;

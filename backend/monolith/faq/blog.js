const express = require("express");
const {
  uploadImage,
  addBlog,
  updateBlog,
  getBlog,
  getBlogs,
  deleteBlog,
} = require("../controllers/Blog");
const Blog = require("../models/Blog");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");

const { protect } = require("../middleware/auth");
router.route("/").get(advancedResults(Blog), getBlogs).post(addBlog);
router.route("/:id").get(getBlog).put(updateBlog).delete(deleteBlog);
router.post("/image", uploadImage);
// router.post("/updateContent/:id", updateContent);

// router.get("/:id", getContent);

module.exports = router;

const express = require("express");
const {
  uploadImage,
  addContact,
  updateContact,
  getContact,
  getContacts,
  deleteContact,
} = require("../controllers/Contact");
const Contact = require("../models/Contact");
const router = express.Router();
const advancedResults = require("../middleware/advancedResults");

const { protect } = require("../middleware/auth");
router.route("/").get(advancedResults(Contact), getContacts).post(addContact);
router.route("/:id").get(getContact).put(updateContact).delete(deleteContact);
router.post("/image", uploadImage);
// router.post("/updateContent/:id", updateContent);

// router.get("/:id", getContent);

module.exports = router;

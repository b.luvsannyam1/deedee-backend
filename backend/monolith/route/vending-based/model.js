const express = require("express");
const {
  addModel,
  updateModel,
  getModel,
  getModels,
  deleteModel,
} = require("../../controller/vending-based/model");
const Model = require("../../model/vending-based/Model");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router.route("/").get(advancedResults(Model, getModels)).post(addModel);
router.route("/:id").get(getModel).put(updateModel).delete(deleteModel);

module.exports = router;

const express = require("express");
const {
  addProduct,
  updateProduct,
  getProduct,
  getProducts,
  deleteProduct,
  ImageHandler,
  getHelp,
} = require("../../controller/product-based/product");
const router = express.Router();
const advancedProducts = require("../../middleware/advencedProducts");
// const { protect } = require("../../middleware/auth");
router.route("/help").get(getHelp);
router
  .route("/")
  .get(
    advancedProducts(["brand", "manufacturer", "type", "filters", "boundary"]),
    getProducts
  )
  .post(addProduct);
router.route("/:id").get(getProduct).put(updateProduct).delete(deleteProduct);
router.route("/image").post(ImageHandler);
module.exports = router;

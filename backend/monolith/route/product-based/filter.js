const express = require("express");
const router = express.Router();
const {
  addFilter,
  updateFilter,
  getFilter,
  getFilters,
  deleteFilter,
} = require("../../controller/product-based/filter");
const Filter = require("../../model/product-based/Filter");
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router.route("/").get(advancedResults(Filter), getFilters).post(addFilter);
router.route("/:id").get(getFilter).put(updateFilter).delete(deleteFilter);

module.exports = router;

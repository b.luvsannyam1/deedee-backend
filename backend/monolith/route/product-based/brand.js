const express = require("express");
const {
  addBrand,
  updateBrand,
  getBrand,
  getBrands,
  deleteBrand,
  ImageHandler,
} = require("../../controller/product-based/brand");
const Brand = require("../../model/product-based/Brand");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");
const { protect } = require("../../middleware/auth");

router.route("/").get(advancedResults(Brand), getBrands).post(addBrand);
router.route("/:id").get(getBrand).put(updateBrand).delete(deleteBrand);
router.route("/image").post(ImageHandler);
module.exports = router;

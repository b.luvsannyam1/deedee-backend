const express = require("express");
const {
  addProductCategory,
  updateProductCategory,
  getProductCategory,
  getProductCategorys,
  deleteProductCategory,
} = require("../../controller/product-based/productcategory");
const ProductCategory = require("../../model/product-based/ProductCategory");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

// const { protect } = require("../../middleware/auth");
router
  .route("/")
  .get(advancedResults(ProductCategory), getProductCategorys)
  .post(addProductCategory);
router
  .route("/:id")
  .get(getProductCategory)
  .put(updateProductCategory)
  .delete(deleteProductCategory);

module.exports = router;

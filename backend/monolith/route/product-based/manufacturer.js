const express = require("express");
const {
  addManufacturer,
  updateManufacturer,
  getManufacturer,
  getManufacturers,
  deleteManufacturer,
  ImageHandler,
} = require("../../controller/product-based/manufacturer");
const Manufacturer = require("../../model/product-based/Manufacturer");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router
  .route("/")
  .get(advancedResults(Manufacturer), getManufacturers)
  .post(addManufacturer);
router
  .route("/:id")
  .get(getManufacturer)
  .put(updateManufacturer)
  .delete(deleteManufacturer);
router.route("/image").post(ImageHandler);

module.exports = router;

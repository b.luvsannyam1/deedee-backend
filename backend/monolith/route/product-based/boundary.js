const express = require("express");
const {
  addBoundary,
  updateBoundary,
  getBoundary,
  getBoundarys,
  deleteBoundary,
} = require("../../controller/product-based/boundary");
const Boundary = require("../../model/product-based/Boundary");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router
  .route("/")
  .get(advancedResults(Boundary), getBoundarys)
  .post(addBoundary);
router
  .route("/:id")
  .get(getBoundary)
  .put(updateBoundary)
  .delete(deleteBoundary);

module.exports = router;

const express = require("express");
const {
  addConstant,
  updateConstant,
  getConstant,
  getConstants,
  deleteConstant,
} = require("../../controller/web-based/constants");
const Constant = require("../../model/web-based/Constant");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router
  .route("/")
  .get(advancedResults(Constant, ""), getConstants)
  .post(addConstant);
router
  .route("/:id")
  .get(getConstant)
  .put(updateConstant)
  .delete(deleteConstant);

module.exports = router;

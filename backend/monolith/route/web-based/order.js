const express = require("express");
const { SendEmail } = require("../../controller/web-based/order");
const RatioCard = require("../../model/web-based/RatioCard");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router
  .route("/")
  .get(advancedResults(RatioCard), getRatioCards)
  .post(SendEmail);

module.exports = router;

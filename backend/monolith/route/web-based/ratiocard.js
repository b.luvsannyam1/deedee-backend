const express = require("express");
const {
  addRatioCard,
  updateRatioCard,
  getRatioCard,
  getRatioCards,
  deleteRatioCard,
  ImageHandler,
} = require("../../controller/web-based/ratiocard");
const RatioCard = require("../../model/web-based/RatioCard");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router
  .route("/")
  .get(advancedResults(RatioCard), getRatioCards)
  .post(addRatioCard);
router
  .route("/:id")
  .get(getRatioCard)
  .put(updateRatioCard)
  .delete(deleteRatioCard);
router.route("/image").post(ImageHandler);

module.exports = router;

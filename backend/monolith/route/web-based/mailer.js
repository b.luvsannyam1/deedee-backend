const express = require("express");
const { sendEmail } = require("../../controller/web-based/maiiler");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router.route("/").post(sendEmail);

module.exports = router;

const express = require("express");
const {
  addBlog,
  updateBlog,
  getBlog,
  getBlogs,
  deleteBlog,
  ImageHandler,
} = require("../../controller/web-based/blog");
const Blog = require("../../model/web-based/Blog");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router.route("/").get(advancedResults(Blog, "creator"), getBlogs).post(addBlog);
router.route("/:id").get(getBlog).put(updateBlog).delete(deleteBlog);
router.route("/image").post(ImageHandler);
module.exports = router;

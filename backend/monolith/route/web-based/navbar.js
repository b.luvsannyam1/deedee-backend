const express = require("express");
const {
  addNavBar,
  updateNavBar,
  getNavBar,
  getNavBars,
  deleteNavBar,
} = require("../../controller/web-based/navbar");
const NavBar = require("../../model/web-based/NavBar");
const router = express.Router();
const advancedResults = require("../../middleware/advancedResults");

const { protect } = require("../../middleware/auth");
router
  .route("/")
  .get(advancedResults(NavBar, "bottomLinks.children.products"), getNavBars)
  .post(addNavBar);
router.route("/:id").get(getNavBar).put(updateNavBar).delete(deleteNavBar);

module.exports = router;

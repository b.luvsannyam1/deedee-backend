const nodemailer = require("nodemailer");

const sendEmail = async (options) => {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.SMTP_EMAIL,
      pass: process.env.SMTP_PASSWORD,
    },
  });
  console.log(options);
  console.log(options.products.cartItems);
  let order;
  options.products.cartItems.forEach((element) => {
    order = element.name + " -ээс" + element.quantity + " ширхэг";
  });
  const message = {
    from: `${process.env.FROM_NAME} <${process.env.FROM_EMAIL}>`,
    to: "b.luvsannyam1@gmail.com",
    subject: "Шинэ захиалга " + options.pnumber,
    html:
      " Хэрэглэгчийн утасны дугаар " +
      options.pnumber +
      ".Хэрэглэгчийн хаяг " +
      options.address +
      ". Нийт төлөх төлбөр " +
      options.products.total +
      "." +
      order,
  };

  const info = await transporter.sendMail(message);

  console.log("Message sent: %s", info.messageId);
};

module.exports = sendEmail;

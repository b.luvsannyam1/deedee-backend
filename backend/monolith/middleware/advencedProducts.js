const Product = require("../model/product-based/Product");
const advancedResults = (populate, tags) => async (req, res, next) => {
  let query;

  // Copy req.query
  const reqQuery = { ...req.query };
  console.log("QueryStr not manipulated");
  console.log(reqQuery);

  // Fields to exclude
  const removeFields = ["select", "sort", "page", "limit"];

  // Loop over removeFields and delete them from reqQuery
  removeFields.forEach((param) => delete reqQuery[param]);

  // Create query string
  let queryStr = JSON.stringify(reqQuery);
  console.log("queryStr");
  console.log(queryStr);

  // Create operators ($gt, $gte, etc)
  queryStr = queryStr.replace(
    /\b(gt|gte|lt|lte|in|elemMatch)\b/g,
    (match) => `$${match}`
  );
  console.log(JSON.parse(queryStr));
  if (Object.values(queryStr).indexOf("") > -1) {
    console.log("Хоосон");
    res.advancedResults = {
      success: false,
      description: "Query string is equal to null",
    };

    next();
  } else {
    // Finding resource

    //todo Checking if query has multiple values
    queryStr = JSON.parse(queryStr);
    for (const [key, value] of Object.entries(queryStr)) {
      console.log(`${key}: ${value}`);
      let stringArray = value.split(",");
      if (stringArray.length === 1) {
        console.log("Not multi value");
      } else {
        console.log("Multi value");
        console.log(stringArray);
        stringArray = stringArray.map((filterId) => {
          console.log(filterId);
          return filterId;
        });
        queryStr[key] = { $all: stringArray };
      }
    }
    console.log("Үр дүн");
    console.log(queryStr);
    //todo Splitting the value and manipulate it to object form

    query = Product.find(queryStr);
    // Select Fields
    if (req.query.select) {
      const fields = req.query.select.split(",").join(" ");
      query = query.select(fields);
    }

    // Sort
    if (req.query.sort) {
      const sortBy = req.query.sort.split(",").join(" ");
      query = query.sort(sortBy);
    } else {
      query = query.sort("-createdAt");
    }

    // Pagination
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 25;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const total = await Product.countDocuments();

    query = query.skip(startIndex);

    if (populate) {
      query = query.populate(populate);
    }

    // Executing query
    const results = await query;

    // Pagination result
    const pagination = { total: total };

    if (endIndex < total) {
      pagination.next = {
        page: page + 1,
        limit,
      };
    }

    if (startIndex > 0) {
      pagination.prev = {
        page: page - 1,
        limit,
      };
    }

    res.advancedResults = {
      success: true,
      count: results.length,
      pagination,
      data: results,
    };

    next();
  }
};

module.exports = advancedResults;

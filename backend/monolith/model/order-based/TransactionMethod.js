const mongoose = require("mongoose");

const TransactionSchema = new mongoose.Schema({
  name: { type: String, require: [true, "Every method must have a name"] },
  supplier: {
    type: String,
    require: [true, "Every method must have a supplier"],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Transaction", TransactionSchema);

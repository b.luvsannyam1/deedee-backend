const mongoose = require("mongoose");

const TransactionSchema = new mongoose.Schema({
  amount: {
    type: Number,
    require: [true, "Every transaction must have a transaction amount"],
  },
  method: { type: String, enum: ["Lend", "KhanBank"] },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Transaction", TransactionSchema);

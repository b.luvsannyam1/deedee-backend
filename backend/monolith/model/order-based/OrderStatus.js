const mongoose = require("mongoose");

const OrderStatusSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ["requested", "resolved", "failed"],
    required: [true, "Every order status must have a name"],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("OrderStatus", OrderStatusSchema);

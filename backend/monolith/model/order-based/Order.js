const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema({
  total: {
    type: Number,
    required: [true, "Every order must have a total"],
  },

  products: [
    {
      ref: "Product",
      type: mongoose.Schema.ObjectId,
      require: [true, "Every order is a collection of sold products"],
    },
  ],
  status: { enum: ["completed", "deleted", "cancelled"] },
  payment: {
    transactionId: { ref: "Transaction", type: mongoose.Schema.ObjectId },
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Order", OrderSchema);

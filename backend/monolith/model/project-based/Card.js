const mongoose = require("mongoose");

const CardSchema = new mongoose.Schema({
  name: {
    type: String,
    require: [true, "Every project must have a name"],
  },
  description: { type: String },
  idAttachmentCover: { type: String },

  idMembers: [{ ref: "User", type: mongoose.Schema.ObjectId }],
  idLabels: [{ ref: "ProjectLabel", type: mongoose.Schema.ObjectId }],
  attachments: [{ ref: "Activites", type: mongoose.Schema.ObjectId }],
  subscribed: false,
  checklists: [{ type: mongoose.Schema.ObjectId }],

  due: null,
});

module.exports = mongoose.model("Card", CardSchema);

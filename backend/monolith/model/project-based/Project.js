const mongoose = require("mongoose");

const ProjectsSchema = new mongoose.Schema({
  name: {
    type: String,
    require: [true, "Every project must have a name"],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Projects", ProjectsSchema);

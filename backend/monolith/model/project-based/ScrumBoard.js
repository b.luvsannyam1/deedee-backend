const mongoose = require("mongoose");

const ProjectsSchema = new mongoose.Schema({
  name: {
    type: String,
    require: [true, "Every project must have a name"],
  },
  uri: { type: String },
  settings: {
    color: { type: String, default: "fuse-dark" },
    subscribed: { type: Boolean, default: false },
    cardCoverImages: { type: Boolean, default: true },
  },
  list: [
    {
      name: { type: String },
      idCards: { ref: "List", type: mongoose.Schema.ObjectId },
    },
  ],

  members: [{ ref: "User", type: mongoose.Schema.ObjectId }],
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Projects", ProjectsSchema);

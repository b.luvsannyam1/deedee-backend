const mongoose = require("mongoose");

const ToDoSchema = new mongoose.Schema({
  title: { type: String, require: [true, "Every To Do must have a title"] },
  notes: { type: String },
  startDate: {
    type: Date,
    default: Date.now,
  },
  dueDate: {
    type: Date,
  },
  completed: { type: Boolean, default: false },
  starred: { type: Boolean, default: false },
  important: { type: Boolean, default: false },
  deleted: {
    type: Boolean,
    default: false,
  },
  labels: [{ type: mongoose.Schema.ObjectId }],
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("ToDo", ToDoSchema);

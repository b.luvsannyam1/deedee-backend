const mongoose = require("mongoose");

const ProjectFilterSchema = new mongoose.Schema({
  handle: { type: String, require: [true, "Every label must have a handle"] },
  title: { type: String, require: [true, "Every label must have a title"] },
  icon: { type: String, require: [true, "Every label must have a icon"] },
});

module.exports = mongoose.model("ProjectFilter", ProjectFilterSchema);

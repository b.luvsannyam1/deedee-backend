const mongoose = require("mongoose");

const ProjectLabelSchema = new mongoose.Schema({
  handle: { type: String, require: [true, "Every label must have a handle"] },
  title: { type: String, require: [true, "Every label must have a title"] },
  color: { type: String, require: [true, "Every label must have a label"] },
});

module.exports = mongoose.model("ProjectLabel", ProjectLabelSchema);

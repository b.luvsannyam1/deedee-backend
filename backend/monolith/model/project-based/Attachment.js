const mongoose = require("mongoose");

const AttachmentSchema = new mongoose.Schema({
  name: {
    type: String,
    require: [true, "Every project must have a name"],
  },
  src: { type: String, require: [true, "Every project must have a src"] },
  time: { type: Date, default: Date.now() },
  type: { type: String },
});

module.exports = mongoose.model("Attachment", AttachmentSchema);

const mongoose = require("mongoose");

const CardSchema = new mongoose.Schema({
  type: { type: String },
  idMember: { ref: "User", type: mongoose.Schema.ObjectId },
  message: { type: String },
  time: { type: Date, default: Date.now() },
});

module.exports = mongoose.model("Card", CardSchema);

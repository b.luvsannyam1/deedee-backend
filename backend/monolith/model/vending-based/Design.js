const mongoose = require("mongoose");

const DesignSchema = new mongoose.Schema({
  name: { type: String, require: [true, "Every design must have a name"] },
  customCss: { type: String },
  createdAt: { type: Date, default: Date.now() },
});

module.exports = mongoose.model("Design", DesignSchema);

const mongoose = require("mongoose");

const VendingMachineSchema = new mongoose.Schema({
  name: { type: String, require: [true, ""] },
  model: { ref: "Model", type: mongoose.Schema.ObjectId },
  inventory: [
    {
      product: { ref: "Product", type: mongoose.Schema.ObjectId },
      count: { type: Number, default: 10 },
    },
  ],
  maximumCapacity: { type: Number },
  ownedBy: { ref: "Company", type: mongoose.Schema.ObjectId },
  supposedAddress: {
    address: { type: String },
    lat: { type: Number },
    lng: { type: String },
  },
  design: {
    ref: "Design",
    type: mongoose.Schema.ObjectId,
    require: [true, "Every Vending Machine must have a defeault UI design"],
  },
  services: { type: mongoose.Schema.ObjectId },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("VendingMachine", VendingMachineSchema);

const mongoose = require("mongoose");

const Service = new mongoose.Schema({
  name: { type: String, require: [true, "Every service must have a name"] },
  relatedCompany: { type: mongoose.Schema.ObjectId, ref: "Company" },
  serviceType: { type: "String", enum: ["Serial", "Api"] },
});

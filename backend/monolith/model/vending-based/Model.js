const mongoose = require("mongoose");

const ModelSchema = new mongoose.Schema({
  name: { type: String, require: [true, "Every model must have a name"] },
  description: {
    type: String,
    require: [
      true,
      "Every bending machine model has to have a model description",
    ],
  },
});

module.exports = mongoose.model("Model", ModelSchema);

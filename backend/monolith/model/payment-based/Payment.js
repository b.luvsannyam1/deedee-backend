const mongoose = require("mongoose");

const PaymentMethod = new mongoose.Schema({
  name: {
    type: String,
    require: [true, "Every payment method must have a name"],
  },
  apiKey: {
    type: String,
    require: [true, "Every payment method must have a api key"],
  },
  apiBaseAddress: {
    type: String,
    require: [true, "Every payment method needs a base address"],
  },
});

module.exports = mongoose.model("PaymentMethod", PaymentMethod);

const mongoose = require("mongoose");

const ConstantSchema = new mongoose.Schema({
  name: { type: String },
  value: { type: String },
  description: {
    type: String,
  },
});

module.exports = mongoose.model("Constant", ConstantSchema);

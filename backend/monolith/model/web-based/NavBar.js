const mongoose = require("mongoose");

const NavBarSchema = new mongoose.Schema({
  toplinks: [
    {
      title: {
        type: String,
        required: [true, "Link бүр гарчигтай байх ёстой"],
      },
      name: { type: String, required: [true, "Link бүр гарчигтай байх ёстой"] },
      to: { type: String, required: [true, "Link бүр гарчигтай байх ёстой"] },
    },
  ],
  bottomLinks: [
    {
      title: {
        type: String,
        required: [true, "Link бүр гарчигтай байх ёстой"],
      },
      name: { type: String, required: [true, "Link бүр гарчигтай байх ёстой"] },
      to: { type: String, required: [true, "Link бүр гарчигтай байх ёстой"] },
      children: {
        links: [
          {
            title: {
              type: String,
            },
            name: {
              type: String,
            },
            to: {
              type: String,
            },
          },
        ],
      },
      products: {
        type: mongoose.Schema.ObjectId,
        ref: "Product",
      },
    },
  ],
});

module.exports = mongoose.model("NavBar", NavBarSchema);

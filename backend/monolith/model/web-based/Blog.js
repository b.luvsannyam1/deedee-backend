const mongoose = require("mongoose");

const BlogSchema = new mongoose.Schema({
  title: { type: String, require: [true, "Every Blog must have a title"] },
  banner: { type: String },
  description: {
    type: { String },
  },
  content: {
    type: String,
    require: [true, "Every Blog must have a content"],
  },
  featured: { type: Boolean, default: false },
  creator: { ref: "User", type: mongoose.Schema.ObjectId },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Blog", BlogSchema);

const mongoose = require("mongoose");

const RatioCardSchema = new mongoose.Schema({
  name: {
    type: String,
    require: [true, " Big card must have a must have a name"],
  },
  description: {
    type: String,
    require: [true, "Every  Big card must have a description"],
  },
  active: { type: Boolean, default: true },
  cardType: { type: String, enum: ["big", "small", "promotion", "product"] },
  position: { type: String, enum: ["carousel", "featured"] },
  img_src: {
    type: String,
    require: [true, "Big Card  must have a img"],
  },
  linkTo: { type: String, require: [true, "Every Big card must have a link"] },
});

module.exports = mongoose.model("RatioCard", RatioCardSchema);

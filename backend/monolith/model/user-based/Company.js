const mongoose = require("mongoose");

const CompanySchema = new mongoose.Schema(
  {
    displayName: {
      type: String,
      unique: true,
      require: [true, "Every company must have a name"],
    },
    description: {
      type: String,
      require: [true, "Every company must have a description"],
    },
    ownedMachines: [
      {
        ref: "VendingMachines",
        type: mongoose.Schema.ObjectId,
      },
    ],
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: { virtuals: true }, // So `res.json()` and other `JSON.stringify()` functions include virtuals
    toObject: { virtuals: true }, // So `toObject()` output includes virtuals
  }
);

CompanySchema.virtual("members", {
  ref: "User",
  localField: "_id",
  foreignField: "company",
  justOne: true,
  options: { sort: { name: -1 }, limit: 5 },
});

module.exports = mongoose.model("Company", CompanySchema);

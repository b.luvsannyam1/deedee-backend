const crypto = require("crypto");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please add a name"],
  },
  email: {
    type: String,
    required: [true, "Please add an email"],
    unique: true,
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Please add a valid email",
    ],
  },
  photoURL: {
    type: String,
  },
  company: {
    type: mongoose.Schema.ObjectId,
  },
  role: {
    type: String,
    enum: ["admin", "staff", "user"],
    default: "admin",
  },
  settings: {
    layout: {
      style: {
        type: String,
        default: "layout1",
      },
      config: {
        scroll: {
          type: String,
          default: "content",
        },
        navbar: {
          display: {
            type: Boolean,
            default: true,
          },
          folded: {
            type: Boolean,
            default: true,
          },
          position: {
            type: String,
            default: "left",
          },
        },
        toolbar: {
          display: {
            type: Boolean,
            default: true,
          },
          style: {
            type: String,
            default: "fixed",
          },
          position: {
            type: String,
            default: "below",
          },
        },
        footer: {
          display: {
            type: Boolean,
            default: true,
          },
          style: {
            type: String,
            default: "fixed",
          },
          position: {
            type: String,
            default: "below",
          },
        },
        mode: {
          type: String,
          default: "fullwidth",
        },
      },
    },
    customScrollbars: {
      type: Boolean,
      default: true,
    },
    theme: {
      main: {
        type: String,
        default: "defaultDark",
      },
      navbar: {
        type: String,
        default: "defaultDark",
      },
      toolbar: {
        type: String,
        default: "defaultDark",
      },
      footer: {
        type: String,
        default: "defaultDark",
      },
    },
  },
  ownedVendingMachines: {
    ref: "VendingMachine",
    type: mongoose.Schema.ObjectId,
  },
  shortcuts: {
    type: Array,
    default: ["calendar", "mail", "contacts"],
  },
  password: {
    type: String,
    required: [true, "Please add a password"],
    minlength: 6,
    select: false,
  },
  resetPasswordToken: String,
  resetPasswordExpire: Date,
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

// Encrypt password using bcrypt
UserSchema.pre("save", async function (next) {
  if (!this.isModified("password")) {
    next();
  }

  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

// Sign JWT and return
UserSchema.methods.getSignedJwtToken = function () {
  return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRE,
  });
};

// Match user entered password to hashed password in database
UserSchema.methods.matchPassword = async function (enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};

// Generate and hash password token
UserSchema.methods.getResetPasswordToken = function () {
  // Generate token
  const resetToken = crypto.randomBytes(20).toString("hex");

  // Hash token and set to resetPasswordToken field
  this.resetPasswordToken = crypto
    .createHash("sha256")
    .update(resetToken)
    .digest("hex");

  // Set expire
  this.resetPasswordExpire = Date.now() + 10 * 60 * 1000;

  return resetToken;
};

module.exports = mongoose.model("User", UserSchema);

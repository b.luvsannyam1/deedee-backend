const mongoose = require("mongoose");

const BoundarySchema = new mongoose.Schema({
  boundaryName: {
    type: String,
    required: [true, "Boundary болгон өөрийн гэсэн нэртэй байх ёстой"],
    unique: [true, "Boundary-ын нэр давхацах ёсгүй"],
  },
  details: { type: String, required: true },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Boundary", BoundarySchema);

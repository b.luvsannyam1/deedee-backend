const mongoose = require("mongoose");

const BrandSchema = new mongoose.Schema(
  {
    brandName: {
      type: String,
      required: [true, "Brand болгон өөрийн гэсэн нэртэй байх ёстой"],
      unique: [true, "Brand-ын нэр давхацах ёсгүй"],
    },
    details: { type: String, required: true },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  { _id: Number, name: String }
);
BrandSchema.method("transform", function () {
  var obj = this.toObject();

  //Rename fields
  obj.id = obj._id;
  delete obj._id;

  return obj;
});

module.exports = mongoose.model("Brand", BrandSchema);

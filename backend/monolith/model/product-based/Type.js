const mongoose = require("mongoose");

const TypeSchema = new mongoose.Schema({
  typeName: {
    type: String,
    required: [true, "Type болгон өөрийн гэсэн нэртэй байх ёстой"],
    unique: [true, "Type-ын нэр давхацах ёсгүй"],
  },
  details: { type: String, required: true },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Type", TypeSchema);

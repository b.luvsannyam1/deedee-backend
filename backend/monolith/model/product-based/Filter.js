const mongoose = require("mongoose");

const FilterSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Filter болгон өөрийн гэсэн нэртэй байх ёстой"],
    unique: [true, "Filter-ын нэр давхацах ёсгүй"],
  },
  description: {
    type: String,
    required: [true, "Filter болгон өөрийн гэсэн нэртэй байх ёстой"],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Filter", FilterSchema);

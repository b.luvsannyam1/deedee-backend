const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    require: [true, "Product бүр өөрийн нэртэй байна "],
  },
  html: { type: String, default: "<p>Hello</>" },
  brand: {
    type: mongoose.Schema.ObjectId,
    ref: "Brand",
    required: [true, "Product бүр ямар нэг брендтэй байх ёстой"],
  },
  manufacturer: {
    type: mongoose.Schema.ObjectId,
    ref: "Manufacturer",
    required: [true, "Product бүр ямар нэг үйлдвэрлэгчтэй байх ёстой"],
  },
  type: {
    type: mongoose.Schema.ObjectId,
    ref: "Type",
    required: [true, "Product бүр ямар нэг төрлийнх байх ёстой"],
  },
  filters: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "Filter",
    },
  ],
  boundary: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "Boundary",
    },
  ],
  handle: {
    type: String,
  },
  description: [
    {
      type: String,
      default: "Hello",
    },
  ],
  featureOn: {
    type: String,
    enum: ["featured", "performance", "system", "specialseries", "none"],
  },
  featuredImageId: { type: String },
  images: [
    {
      type: String,
      require: [true, "At least one image is required for a product"],
    },
  ],
  weight: {
    type: String,
    require: [true, "Every product should have it's weight written down"],
  },
  activeForUsers: {
    type: Boolean,
    require: [true, "Every product should be specified"],
  },
  sales: {
    type: Number,
    default: 0,
  },
  price: { type: Number, default: 5000 },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Product", ProductSchema);

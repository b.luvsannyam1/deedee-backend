const mongoose = require("mongoose");

const ManufacturerSchema = new mongoose.Schema({
  manufacturerName: {
    type: String,
    required: [true, "Manufacturer болгон өөрийн гэсэн нэртэй байх ёстой"],
    unique: [true, "Manufacturer-ын нэр давхацах ёсгүй"],
  },
  details: { type: String, required: true },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Manufacturer", ManufacturerSchema);

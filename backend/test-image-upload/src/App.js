import logo from "./logo.svg";
import "./App.css";
import React, { useState } from "react";

function App() {
  const [link, setlink] = useState("http://localhost:5000/uploads/blog/01.jpg");
  const ImageOnChange = async (e, onChange) => {
    console.log(e.target.files[0]);
    var formdata = new FormData();
    formdata.append("image", e.target.files[0], e.target.files[0].name);
    console.log(onChange);
    var requestOptions = {
      method: "POST",
      body: formdata,
      redirect: "follow",
    };

    fetch("http://localhost:7000/upload", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result.url);
        setlink(result.url);
      })
      .catch((error) => console.log("error", error));
  };
  return (
    <div className="App">
      <header className="App-header">
        <img src={link} className="App-logo" alt="logo" />
        <input
          accept="image/*"
          type="file"
          id="button-file"
          label="Зураг"
          onChange={(e) => ImageOnChange(e)}
        />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;

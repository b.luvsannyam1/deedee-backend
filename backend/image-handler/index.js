const express = require("express");
const fileUpload = require("express-fileupload");
const cors = require("cors");
const app = express();

// default options
app.use(fileUpload());
const corsOptions = {
  credentials: true,
  origin: "http://localhost:3000",
};

app.use(cors(corsOptions));
app.post("/upload", async function (req, res) {
  let sampleFile;
  let uploadPath;
  await console.log(req.files);
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send("No files were uploaded.");
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  sampleFile = req.files.image;
  uploadPath = __dirname + "/images/" + sampleFile.name;

  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv(uploadPath, function (err) {
    if (err) return res.status(500).send(err);

    res.status(200).json({
      url: "http://localhost:5000/uploads/blog/helloworld.jpg",
      urls: {
        default: "http://localhost:5000/uploads/blog/helloworld.jpg",
      },
    });
  });
});
const PORT = 7000;
app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});
// Handle unhandled promise rejections
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}`);
  // Close server & exit process
  app.close(() => process.exit(1));
});

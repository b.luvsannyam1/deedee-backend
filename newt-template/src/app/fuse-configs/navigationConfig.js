import { authRoles } from "app/auth";
import i18next from "i18next";
import DocumentationNavigation from "../main/documentation/DocumentationNavigation";

import en from "./navigation-i18n/en";
import mn from "./navigation-i18n/mn";

i18next.addResourceBundle("mn", "navigation", mn);
i18next.addResourceBundle("en", "navigation", en);

const navigationConfig = [
  {
    id: "applications",
    title: "Applications",
    translate: "APPLICATIONS",
    type: "group",
    icon: "apps",
    children: [
      {
        id: "dashboards",
        title: "Dashboards",
        translate: "DASHBOARDS",
        type: "collapse",
        icon: "dashboard",
        children: [
          {
            id: "analytics-dashboard",
            title: "Analytics",
            translate: "ANALYTICS",
            type: "item",
            url: "/apps/dashboards/analytics",
          },
          {
            id: "project-dashboard",
            title: "Project",
            translate: "PROJECT",
            type: "item",
            auth: [authRoles.admin, authRoles.staff],
            url: "/apps/dashboards/project",
          },
        ],
      },
      //NAVBAR Company
      {
        id: "company",
        title: "Company",
        translate: "COMPANY",
        type: "collapse",
        icon: "shopping_cart",
        url: "/apps/company",
        children: [
          {
            id: "company-analytics-dashboard",
            title: "Company Analytics",
            translate: "COMPANY_ANALYTICS",
            type: "item",
            url: "/apps/company",
          },
          {
            id: "e-commerce-product-detail",
            title: "Product Detail",
            translate: "PRODUCT_DETAIL",
            type: "item",
            url: "/apps/e-commerce/products/1/a-walk-amongst-friends-canvas-print",
            exact: true,
          },
          {
            id: "e-commerce-new-product",
            title: "New Product",
            translate: "NEW_PRODUCT",
            type: "item",
            url: "/apps/e-commerce/products/new",
            exact: true,
          },
        ],
      },

      //NAVBAR PRODUCTS
      {
        id: "e-commerce",
        title: "E-Commerce",
        translate: "ECOMMERCE",
        type: "collapse",
        icon: "shopping_cart",
        url: "/apps/e-commerce",
        children: [
          {
            id: "e-commerce-products",
            title: "Product",
            translate: "PRODUCT",
            type: "collapse",
            url: "/apps/e-commerce/products",
            exact: true,
            children: [
              {
                id: "e-commerce-products",
                title: "Products",
                translate: "PRODUCTS",
                type: "item",
                url: "/apps/e-commerce/products",
                exact: true,
              },

              {
                id: "e-commerce-new-product",
                title: "New Product",
                translate: "NEW_PRODUCT",
                type: "item",
                url: "/apps/e-commerce/products/new",
                exact: true,
              },
            ],
          },

          {
            id: "e-commerce-brand",
            title: "Brand",
            translate: "BRAND",
            type: "collapse",
            url: "/apps/brands/brands",
            exact: true,
            children: [
              {
                id: "e-commerce-brands",
                title: "Brands",
                translate: "BRANDS",
                type: "item",
                url: "/apps/brands/brands",
                exact: true,
              },
              {
                id: "e-commerce-new-brand",
                title: "New brand",
                translate: "NEW_BRAND",
                type: "item",
                url: "/apps/brands/brands/new",
                exact: true,
              },
            ],
          },
          {
            id: "e-commerce-manufacturer",
            title: "Manufacturer",
            translate: "MANUFACTURER",
            type: "collapse",
            url: "/apps/manufacturers/manufacturers",
            exact: true,
            children: [
              {
                id: "e-commerce-manufacturer",
                title: "Manufacturer",
                translate: "MANUFACTURER",
                type: "item",
                url: "/apps/manufacturers/manufacturers",
                exact: true,
              },

              {
                id: "e-commerce-new-product",
                title: "New Manufacturer",
                translate: "NEW_MANUFACTURER",
                type: "item",
                url: "/apps/manufacturers/manufacturers/new",
                exact: true,
              },
            ],
          },
          {
            id: "e-commerce-type",
            title: "Type",
            translate: "TYPE",
            type: "collapse",
            url: "/apps/types/types",
            exact: true,
            children: [
              {
                id: "e-commerce-type",
                title: "Type",
                translate: "TYPE",
                type: "item",
                url: "/apps/types/types",
                exact: true,
              },
              {
                id: "e-commerce-new-type",
                title: "New type",
                translate: "NEW_TYPE",
                type: "item",
                url: "/apps/types/types/new",
                exact: true,
              },
            ],
          },
          {
            id: "e-commerce-filters",
            title: "Filters",
            translate: "FILTERS",
            type: "collapse",
            url: "/apps/filters/filters",
            exact: true,
            children: [
              {
                id: "e-commerce-filters",
                title: "Filters",
                translate: "FILTERS",
                type: "item",
                url: "/apps/filters/filters",
                exact: true,
              },
              {
                id: "e-commerce-new-filters",
                title: "New filters",
                translate: "NEW_FILTERS",
                type: "item",
                url: "/apps/filters/filters/new",
                exact: true,
              },
            ],
          },
          {
            id: "e-commerce-boundary",
            title: "Boundary",
            translate: "BOUNDARY",
            type: "collapse",
            url: "/apps/boundaries/boundaries",
            exact: true,
            children: [
              {
                id: "e-commerce-boundary",
                title: "Boundary",
                translate: "BOUNDARY",
                type: "item",
                url: "/apps/boundaries/boundaries",
                exact: true,
              },
              {
                id: "e-commerce-new-boundary",
                title: "New boundary",
                translate: "NEW_BOUNDARY",
                type: "item",
                url: "/apps/boundaries/boundaries/new",
                exact: true,
              },
            ],
          },
          {
            id: "e-commerce-category",
            title: "Category",
            translate: "CATEGORY",
            type: "collapse",
            url: "/apps/category/categories",
            exact: true,
            children: [
              {
                id: "e-commerce-category",
                title: "Category",
                translate: "CATEGORY",
                type: "item",
                url: "/apps/category/categories",
                exact: true,
              },
              {
                id: "e-commerce-new-category",
                title: "New category",
                translate: "NEW_CATEGORY",
                type: "item",
                url: "/apps/category/categories/new",
                exact: true,
              },
            ],
          },
        ],
      },

      //NAVBAR OWNED VENDING MACHINES
      // {
      //   id: "e-commerce",
      //   title: "Vending Machines",
      //   translate: "VENDING",
      //   type: "collapse",
      //   icon: "point_of_sale",
      //   url: "/apps/vending-machines",
      //   children: [
      //     {
      //       id: "vending-machines-products",
      //       title: "Machines",
      //       translate: "MACHINES",
      //       type: "item",
      //       auth: authRoles.user,
      //       url: "/apps/vending-machines/products",
      //       exact: true,
      //     },
      //     {
      //       id: "vending-machines-products",
      //       title: "All Machines",
      //       auth: authRoles.admin,
      //       translate: "MACHINES",
      //       type: "item",
      //       url: "/apps/vending-machines/products",
      //       exact: true,
      //     },
      //     {
      //       id: "vending-machines-product-detail",
      //       title: "machine Details",
      //       translate: "MACHINE_DETAILS",
      //       type: "item",
      //       url: "/apps/vending-machines/products/1/a-walk-amongst-friends-canvas-print",
      //       exact: true,
      //     },
      //     {
      //       id: "vending-machines-new-product",
      //       title: "New Machine",
      //       translate: "NEW_MACHINE",
      //       type: "item",
      //       url: "/apps/vending-machines/products/new",
      //       exact: true,
      //     },
      //   ],
      // },
      //CONTACTS
      {
        id: "contacts",
        title: "Web",
        translate: "WEB",
        type: "item",
        auth: authRoles.admin,
        icon: "verified_user",
        url: "/apps/web-configs/all",
      },
      {
        id: "contacts",
        title: "Contacts",
        translate: "CONTACTS",
        type: "item",
        auth: authRoles.admin,
        icon: "verified_user",
        url: "/apps/contacts/all",
      },
    ],
  },

  {
    type: "divider",
    id: "divider-1",
  },
  {
    id: "web",
    title: "Web",
    translate: "WEB",
    type: "collapse",
    auth: authRoles.staff,
    icon: "verified_user",
    url: "/apps/web-configs/all",
    children: [
      {
        id: "cards",
        title: "Cards",
        translate: "CARDS",
        type: "collapse",
        url: "/apps/web-config/cards",
        exact: true,
        children: [
          {
            id: "cards-all",
            title: "All",
            translate: "ALL",
            type: "item",
            url: "/apps/web-config/cards",
            exact: true,
          },
          {
            id: "cards-new",
            title: "New card",
            translate: "NEW_CARD",
            type: "item",
            url: "/apps/web-config/cards/new",
            exact: true,
          },
        ],
      },
      {
        id: "blogs",
        title: "Blog",
        translate: "BLOGS",
        type: "collapse",
        url: "/apps/web-config/blog",
        exact: true,
        children: [
          {
            id: "blogs-all",
            title: "All",
            translate: "ALL",
            type: "item",
            url: "/apps/web-config/blog",
            exact: true,
          },
          {
            id: "blogs-new",
            title: "New blog",
            translate: "NEW_BLOG",
            type: "item",
            url: "/apps/web-config/blog/new",
            exact: true,
          },
        ],
      },
      // {
      //   id: "constant",
      //   title: "Constant",
      //   translate: "CONSTANT",
      //   type: "item",
      //   url: "/apps/web-config/constant",
      //   exact: true,
      // },
    ],
  },
  {
    type: "divider",
    id: "divider-1",
  },
  // {
  //   id: "auth",
  //   title: "Auth",
  //   type: "group",
  //   icon: "verified_user",
  //   children: [
  //     {
  //       id: "login",
  //       title: "Login",
  //       type: "item",
  //       url: "/login",
  //       auth: authRoles.onlyGuest,
  //       icon: "lock",
  //     },
  //     {
  //       id: "register",
  //       title: "Register",
  //       type: "item",
  //       url: "/register",
  //       auth: authRoles.onlyGuest,
  //       icon: "person_add",
  //     },
  //     {
  //       id: "logout",
  //       title: "Logout",
  //       type: "item",
  //       auth: authRoles.user,
  //       url: "/logout",
  //       icon: "exit_to_app",
  //     },
  //   ],
  // },
  // DocumentationNavigation,
];

export default navigationConfig;

import FusePageCarded from "@fuse/core/FusePageCarded";
import withReducer from "app/store/withReducer";
import reducer from "../store";
import ManufacturersHeader from "./ManufacturersHeader";
import ManufacturersTable from "./ManufacturersTable";

function Manufacturers() {
  return (
    <FusePageCarded
      classes={{
        content: "flex",
        contentCard: "overflow-hidden",
        header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
      }}
      header={<ManufacturersHeader />}
      content={<ManufacturersTable />}
      innerScroll
    />
  );
}

export default withReducer("Manufacturers", reducer)(Manufacturers);

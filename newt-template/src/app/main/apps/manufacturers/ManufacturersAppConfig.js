import { lazy } from "react";
import { Redirect } from "react-router-dom";

const ManufacturersAppConfig = {
  settings: {
    layout: {},
  },
  routes: [
    {
      path: "/apps/manufacturers/manufacturers/:manufacturerId",
      component: lazy(() => import("./manufacturer/Manufacturer")),
    },
    {
      path: "/apps/manufacturers/manufacturers",
      component: lazy(() => import("./manufacturers/Manufacturers")),
    },
    {
      path: "/apps/manufacturers",
      component: () => <Redirect to="/apps/manufacturers/manufacturers" />,
    },
  ],
};

export default ManufacturersAppConfig;

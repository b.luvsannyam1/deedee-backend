import FuseLoading from "@fuse/core/FuseLoading";
import FusePageCarded from "@fuse/core/FusePageCarded";
import { useDeepCompareEffect } from "@fuse/hooks";
import Button from "@material-ui/core/Button";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import withReducer from "app/store/withReducer";
import { motion } from "framer-motion";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import _ from "@lodash";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import {
  resetManufacturer,
  newManufacturer,
  getManufacturer,
} from "../store/manufacturerSlice";
import reducer from "../store";
import ManufacturerHeader from "./ManufacturerHeader";
import BasicInfoTab from "./tabs/BasicInfoTab";
import InventoryTab from "./tabs/InventoryTab";
import PricingTab from "./tabs/PricingTab";
import ManufacturerImagesTab from "./tabs/ManufacturerImagesTab";
import ShippingTab from "./tabs/ShippingTab";

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
  name: yup
    .string()
    .required("You must enter a manufacturer name")
    .min(5, "The manufacturer name must be at least 5 characters"),
});

function Manufacturer(props) {
  const dispatch = useDispatch();
  const manufacturer = useSelector(
    ({ Manufacturer }) => Manufacturer.manufacturer
  );

  const routeParams = useParams();
  const [tabValue, setTabValue] = useState(0);
  const [noManufacturer, setNoManufacturer] = useState(false);
  const methods = useForm({
    mode: "onChange",
    defaultValues: {},
    resolver: yupResolver(schema),
  });
  const { reset, watch, control, onChange, formState } = methods;
  const form = watch();

  useDeepCompareEffect(() => {
    function updateManufacturerState() {
      const { manufacturerId } = routeParams;
      console.log(manufacturerId);

      if (manufacturerId === "new") {
        /**
         * Create New Manufacturer data
         */
        dispatch(newManufacturer());
      } else {
        /**
         * Get Manufacturer data
         */
        dispatch(getManufacturer(manufacturerId)).then((action) => {
          /**
           * If the requested manufacturer is not exist show message
           */
          if (!action.payload) {
            setNoManufacturer(true);
          }
        });
      }
    }

    updateManufacturerState();
  }, [dispatch, routeParams]);

  useEffect(() => {
    if (!manufacturer) {
      return;
    }
    /**
     * Reset the form on manufacturer state changes
     */
    reset(manufacturer);
  }, [manufacturer, reset]);

  useEffect(() => {
    return () => {
      /**
       * Reset Manufacturer on component unload
       */
      dispatch(resetManufacturer());
      setNoManufacturer(false);
    };
  }, [dispatch]);

  /**
   * Tab Change
   */
  function handleTabChange(event, value) {
    setTabValue(value);
  }

  /**
   * Show Message if the requested manufacturers is not exists
   */
  if (noManufacturer) {
    return (
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1, transition: { delay: 0.1 } }}
        className="flex flex-col flex-1 items-center justify-center h-full"
      >
        <Typography color="textSecondary" variant="h5">
          There is no such manufacturer!
        </Typography>
        <Button
          className="mt-24"
          component={Link}
          variant="outlined"
          to="/apps/manufacturers/manufacturers"
          color="inherit"
        >
          Go to Manufacturers Page
        </Button>
      </motion.div>
    );
  }

  /**
   * Wait while manufacturer data is loading and form is setted
   */
  if (
    _.isEmpty(form) ||
    (manufacturer &&
      routeParams.manufacturerId !== manufacturer._id &&
      routeParams.manufacturerId !== "new")
  ) {
    return <FuseLoading />;
  }

  return (
    <FormProvider {...methods}>
      <FusePageCarded
        classes={{
          toolbar: "p-0",
          header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
        }}
        header={<ManufacturerHeader />}
        contentToolbar={
          <Tabs
            value={tabValue}
            onChange={handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            classes={{ root: "w-full h-64" }}
          >
            <Tab className="h-64" label="Basic Info" />
          </Tabs>
        }
        content={
          <div className="p-16 sm:p-24 max-w-2xl">
            <div className={tabValue !== 0 ? "hidden" : ""}>
              <BasicInfoTab />
            </div>
          </div>
        }
        innerScroll
      />
    </FormProvider>
  );
}

export default withReducer("Manufacturer", reducer)(Manufacturer);

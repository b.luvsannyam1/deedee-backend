import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getManufacturers = createAsyncThunk(
  "Manufacturers/manufacturers/getManufacturers",
  async () => {
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/manufacturer/"
    );
    const data = await response.data.data;
    console.log(data);
    return data;
  }
);

export const removeManufacturers = createAsyncThunk(
  "Manufacturers/manufacturers/removeManufacturers",
  async (productIds, { dispatch, getState }) => {
    await axios.post("/api/e-commerce-app/remove-manufacturers", {
      productIds,
    });

    return productIds;
  }
);

const manufacturersAdapter = createEntityAdapter({
  // Assume IDs are stored in a field other than `book.id`
  selectId: (manufacturer) => manufacturer._id,
  // Keep the "all IDs" array sorted based on book titles
  sortComparer: (a, b) => a.manufacturerName.localeCompare(b.manufacturerName),
});

export const {
  selectAll: selectManufacturers,
  selectById: selectManufacturerById,
} = manufacturersAdapter.getSelectors(
  (state) => state.Manufacturers.manufacturers
);

const manufacturersSlice = createSlice({
  name: "Manufacturers/manufacturers",
  initialState: manufacturersAdapter.getInitialState({
    searchText: "",
  }),
  reducers: {
    setManufacturersSearchText: {
      reducer: (state, action) => {
        state.searchText = action.payload;
      },
      prepare: (event) => ({ payload: event.target.value || "" }),
    },
  },
  extraReducers: {
    [getManufacturers.fulfilled]: manufacturersAdapter.setAll,
    [removeManufacturers.fulfilled]: (state, action) =>
      manufacturersAdapter.removeMany(state, action.payload),
  },
});

export const { setManufacturersSearchText } = manufacturersSlice.actions;

export default manufacturersSlice.reducer;

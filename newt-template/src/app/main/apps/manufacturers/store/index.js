import { combineReducers } from "@reduxjs/toolkit";
import manufacturer from "./manufacturerSlice";
import manufacturers from "./manufacturersSlice";

const reducer = combineReducers({
  manufacturer,
  manufacturers,
});

export default reducer;

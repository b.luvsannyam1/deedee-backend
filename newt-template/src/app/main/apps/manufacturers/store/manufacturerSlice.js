import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import FuseUtils from "@fuse/utils";

export const getManufacturer = createAsyncThunk(
  "Manufacturers/manufacturer/getManufacturer",
  async (params) => {
    console.log(params);
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/manufacturer/" + params
    );
    const data = await response.data.data;
    console.log(data);

    return data === undefined ? null : data;
  }
);

export const removeManufacturer = createAsyncThunk(
  "Manufacturers/manufacturer/removeManufacturer",
  async (val, { dispatch, getState }) => {
    const { _id } = getState().Manufacturer.manufacturer;
    await axios.delete(
      "http://localhost:5000/server/v1/admin/manufacturer/" + _id
    );

    return _id;
  }
);

export const saveManufacturer = createAsyncThunk(
  "Manufacturers/manufacturer/saveManufacturer",
  async (manufacturerData, { dispatch, getState }) => {
    const { manufacturer } = getState().Manufacturer;

    let data;
    console.log(manufacturer._id);
    if (manufacturer._id) {
      const response = await axios.put(
        "http://localhost:5000/server/v1/admin/manufacturer/" +
          manufacturer._id,
        {
          ...manufacturer,
          ...manufacturerData,
        }
      );
      data = await response.data;
    } else {
      const response = await axios.post(
        "http://localhost:5000/server/v1/admin/manufacturer/",
        {
          ...manufacturer,
          ...manufacturerData,
        }
      );
      data = await response.data[0];
    }
    return data;
  }
);

const manufacturerSlice = createSlice({
  name: "Manufacturers/manufacturer",
  initialState: null,
  reducers: {
    resetManufacturer: () => null,
    newManufacturer: {
      reducer: (state, action) => action.payload,
      prepare: (event) => ({
        payload: {
          icon: "",
          manufacturerName: "",
          details: "",
        },
      }),
    },
  },
  extraReducers: {
    [getManufacturer.fulfilled]: (state, action) => action.payload,
    [saveManufacturer.fulfilled]: (state, action) => action.payload,
    [removeManufacturer.fulfilled]: (state, action) => null,
  },
});

export const { newManufacturer, resetManufacturer } = manufacturerSlice.actions;

export default manufacturerSlice.reducer;

import FusePageCarded from "@fuse/core/FusePageCarded";
import withReducer from "app/store/withReducer";
import reducer from "../store";
import BrandsHeader from "./BrandsHeader";
import BrandsTable from "./BrandsTable";

function Brands() {
  return (
    <FusePageCarded
      classes={{
        content: "flex",
        contentCard: "overflow-hidden",
        header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
      }}
      header={<BrandsHeader />}
      content={<BrandsTable />}
      innerScroll
    />
  );
}

export default withReducer("Brands", reducer)(Brands);

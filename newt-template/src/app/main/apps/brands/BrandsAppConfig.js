import { lazy } from "react";
import { Redirect } from "react-router-dom";

const BrandsAppConfig = {
  settings: {
    layout: {},
  },
  routes: [
    {
      path: "/apps/brands/brands/:productId/:productHandle?",
      component: lazy(() => import("./brand/Brand")),
    },
    {
      path: "/apps/brands/brands",
      component: lazy(() => import("./brands/Brands")),
    },
  ],
};

export default BrandsAppConfig;

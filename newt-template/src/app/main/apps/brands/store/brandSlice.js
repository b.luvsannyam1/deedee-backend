import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import FuseUtils from "@fuse/utils";

export const getBrand = createAsyncThunk(
  "Brand/brand/getBrand",
  async (params) => {
    console.log(params);
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/brand/" + params
    );
    const data = await response.data.data.brand;
    console.log(data);

    return data === undefined ? null : data;
  }
);

export const removeBrand = createAsyncThunk(
  "Brand/brand/removeBrand",
  async (val, { dispatch, getState }) => {
    const { _id } = getState().Brand.brand;
    await axios.delete("http://localhost:5000/server/v1/admin/brand/" + _id);

    return id;
  }
);

export const saveBrand = createAsyncThunk(
  "Brand/brand/saveBrand",
  async (brandData, { dispatch, getState }) => {
    const { brand } = getState().Brand;
    console.log(brandData);
    let data;
    if (brand._id) {
      const response = await axios.put(
        "http://localhost:5000/server/v1/admin/brand/" + brand._id,
        {
          ...brand,
          ...brandData,
        }
      );
      data = await response.data;
    } else {
      const response = await axios.post(
        "http://localhost:5000/server/v1/admin/brand/",
        {
          ...brand,
          ...brandData,
        }
      );
      data = await response.data[0];
    }

    return data;
  }
);

const brandSlice = createSlice({
  name: "Brand/brand",
  initialState: null,
  reducers: {
    resetBrand: () => null,
    newBrand: {
      reducer: (state, action) => action.payload,
      prepare: (event) => ({
        payload: {
          id: FuseUtils.generateGUID(),
          img_src: "",
          typeName: "",
          details: "",
          inBuild: [],
          html: "",
        },
      }),
    },
  },
  extraReducers: {
    [getBrand.fulfilled]: (state, action) => action.payload,
    [saveBrand.fulfilled]: (state, action) => action.payload,
    [removeBrand.fulfilled]: (state, action) => null,
  },
});

export const { newBrand, resetBrand } = brandSlice.actions;

export default brandSlice.reducer;

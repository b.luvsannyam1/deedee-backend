import { combineReducers } from "@reduxjs/toolkit";
import brand from "./brandSlice";
import brands from "./brandsSlice";

const reducer = combineReducers({
  brands,
  brand,
});

export default reducer;

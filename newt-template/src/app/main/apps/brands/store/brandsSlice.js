import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getBrands = createAsyncThunk(
  "Brand/brands/getBrands",
  async () => {
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/brand/"
    );
    const data = await response.data.data;
    console.log(data);
    return data;
  }
);

export const removeBrands = createAsyncThunk(
  "Brand/brands/removeBrands",
  async (brandIds, { dispatch, getState }) => {
    await axios.post("/api/e-commerce-app/remove-brands", { brandIds });

    return brandIds;
  }
);

const brandsAdapter = createEntityAdapter({
  // Assume IDs are stored in a field other than `book.id`
  selectId: (book) => book._id,
  // Keep the "all IDs" array sorted based on book titles
  sortComparer: (a, b) => a.brandName.localeCompare(b.brandName),
});
// const brandsAdapter = createEntityAdapter({});

export const { selectAll: selectBrands, selectById: selectBrandById } =
  brandsAdapter.getSelectors((state) => state.Brands.brands);

const brandsSlice = createSlice({
  name: "Brand/brands",
  initialState: brandsAdapter.getInitialState({
    searchText: "",
  }),
  reducers: {
    setBrandsSearchText: {
      reducer: (state, action) => {
        state.searchText = action.payload;
      },
      prepare: (event) => ({ payload: event.target.value || "" }),
    },
  },
  extraReducers: {
    [getBrands.fulfilled]: brandsAdapter.setAll,
    [removeBrands.fulfilled]: (state, action) =>
      brandsAdapter.removeMany(state, action.payload),
  },
});

export const { setBrandsSearchText } = brandsSlice.actions;

export default brandsSlice.reducer;

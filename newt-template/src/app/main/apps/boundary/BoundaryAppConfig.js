import { lazy } from "react";
import { Redirect } from "react-router-dom";

const ECommerceAppConfig = {
  settings: {
    layout: {},
  },
  routes: [
    {
      path: "/apps/boundaries/boundaries/:boundaryId",
      component: lazy(() => import("./boundary/Boundary")),
    },
    {
      path: "/apps/boundaries/boundaries",
      component: lazy(() => import("./boundaries/Boundaries")),
    },
    {
      path: "/apps/boundaries",
      component: () => <Redirect to="/apps/boundary/boundaries" />,
    },
  ],
};

export default ECommerceAppConfig;

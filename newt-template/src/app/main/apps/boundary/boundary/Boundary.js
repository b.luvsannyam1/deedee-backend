import FuseLoading from "@fuse/core/FuseLoading";
import FusePageCarded from "@fuse/core/FusePageCarded";
import { useDeepCompareEffect } from "@fuse/hooks";
import Button from "@material-ui/core/Button";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import withReducer from "app/store/withReducer";
import { motion } from "framer-motion";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import _ from "@lodash";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import {
  resetBoundary,
  newBoundary,
  getBoundary,
} from "../store/boundarySlice";
import reducer from "../store";
import BoundaryHeader from "./BoundaryHeader";
import BasicInfoTab from "./tabs/BasicInfoTab";
import InventoryTab from "./tabs/InventoryTab";
import PricingTab from "./tabs/PricingTab";
import BoundaryImagesTab from "./tabs/BoundaryImagesTab";
import ShippingTab from "./tabs/ShippingTab";

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
  name: yup
    .string()
    .required("You must enter a boundary name")
    .min(5, "The boundary name must be at least 5 characters"),
});

function Boundary(props) {
  const dispatch = useDispatch();
  const boundary = useSelector(({ Boundary }) => Boundary.boundary);

  const routeParams = useParams();
  const [tabValue, setTabValue] = useState(0);
  const [noBoundary, setNoBoundary] = useState(false);
  const methods = useForm({
    mode: "onChange",
    defaultValues: {},
    resolver: yupResolver(schema),
  });
  const { reset, watch, control, onChange, formState } = methods;
  const form = watch();

  useDeepCompareEffect(() => {
    function updateBoundaryState() {
      const { boundaryId } = routeParams;
      console.log(boundaryId);
      if (boundaryId === "new") {
        /**
         * Create New Boundary data
         */
        dispatch(newBoundary());
      } else {
        /**
         * Get Boundary data
         */
        dispatch(getBoundary(boundaryId)).then((action) => {
          /**
           * If the requested boundary is not exist show message
           */
          if (!action.payload) {
            setNoBoundary(true);
          }
        });
      }
    }

    updateBoundaryState();
  }, [dispatch, routeParams]);

  useEffect(() => {
    if (!boundary) {
      return;
    }
    /**
     * Reset the form on boundary state changes
     */
    reset(boundary);
  }, [boundary, reset]);

  useEffect(() => {
    return () => {
      /**
       * Reset Boundary on component unload
       */
      dispatch(resetBoundary());
      setNoBoundary(false);
    };
  }, [dispatch]);

  /**
   * Tab Change
   */
  function handleTabChange(event, value) {
    setTabValue(value);
  }

  /**
   * Show Message if the requested boundarys is not exists
   */
  if (noBoundary) {
    return (
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1, transition: { delay: 0.1 } }}
        className="flex flex-col flex-1 items-center justify-center h-full"
      >
        <Typography color="textSecondary" variant="h5">
          There is no such boundary!
        </Typography>
        <Button
          className="mt-24"
          component={Link}
          variant="outlined"
          to="/apps/e-commerce/boundarys"
          color="inherit"
        >
          Go to Boundarys Page
        </Button>
      </motion.div>
    );
  }

  /**
   * Wait while boundary data is loading and form is setted
   */
  if (
    _.isEmpty(form) ||
    (boundary &&
      routeParams.boundaryId !== boundary._id &&
      routeParams.boundaryId !== "new")
  ) {
    return <FuseLoading />;
  }

  return (
    <FormProvider {...methods}>
      <FusePageCarded
        classes={{
          toolbar: "p-0",
          header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
        }}
        header={<BoundaryHeader />}
        contentToolbar={
          <Tabs
            value={tabValue}
            onChange={handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            classes={{ root: "w-full h-64" }}
          >
            <Tab className="h-64" label="Basic Info" />
          </Tabs>
        }
        content={
          <div className="p-16 sm:p-24 max-w-2xl">
            <div className={tabValue !== 0 ? "hidden" : ""}>
              <BasicInfoTab />
            </div>
          </div>
        }
        innerScroll
      />
    </FormProvider>
  );
}

export default withReducer("Boundary", reducer)(Boundary);

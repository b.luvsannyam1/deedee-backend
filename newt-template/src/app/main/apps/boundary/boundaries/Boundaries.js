import FusePageCarded from "@fuse/core/FusePageCarded";
import withReducer from "app/store/withReducer";
import reducer from "../store";
import BoundariesHeader from "./BoundariesHeader";
import BoundariesTable from "./BoundariesTable";

function Boundaries() {
  return (
    <FusePageCarded
      classes={{
        content: "flex",
        contentCard: "overflow-hidden",
        header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
      }}
      header={<BoundariesHeader />}
      content={<BoundariesTable />}
      innerScroll
    />
  );
}

export default withReducer("Boundaries", reducer)(Boundaries);

import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getBoundaries = createAsyncThunk(
  "Boundaries/boundaries/getBoundaries",
  async () => {
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/boundary/"
    );
    const data = await response.data.data;
    console.log(data);
    return data;
  }
);

export const removeBoundaries = createAsyncThunk(
  "Boundaries/boundaries/removeBoundaries",
  async (boundaryIds, { dispatch, getState }) => {
    await axios.post("/api/e-commerce-app/remove-boundaries", { boundaryIds });

    return boundaryIds;
  }
);

const boundariesAdapter = createEntityAdapter({
  // Assume IDs are stored in a field other than `book.id`
  selectId: (book) => book._id,
  // Keep the "all IDs" array sorted based on book titles
  sortComparer: (a, b) => a.boundaryName.localeCompare(b.boundaryName),
});

export const { selectAll: selectBoundaries, selectById: selectBoundaryById } =
  boundariesAdapter.getSelectors((state) => state.Boundaries.boundaries);

const boundariesSlice = createSlice({
  name: "Boundaries/boundaries",
  initialState: boundariesAdapter.getInitialState({
    searchText: "",
  }),
  reducers: {
    setBoundariesSearchText: {
      reducer: (state, action) => {
        state.searchText = action.payload;
      },
      prepare: (event) => ({ payload: event.target.value || "" }),
    },
  },
  extraReducers: {
    [getBoundaries.fulfilled]: boundariesAdapter.setAll,
    [removeBoundaries.fulfilled]: (state, action) =>
      boundariesAdapter.removeMany(state, action.payload),
  },
});

export const { setBoundariesSearchText } = boundariesSlice.actions;

export default boundariesSlice.reducer;

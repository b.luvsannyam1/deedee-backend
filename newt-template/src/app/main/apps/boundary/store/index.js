import { combineReducers } from "@reduxjs/toolkit";

import boundary from "./boundarySlice";
import boundaries from "./boundariesSlice";

const reducer = combineReducers({
  boundaries,
  boundary,
});

export default reducer;

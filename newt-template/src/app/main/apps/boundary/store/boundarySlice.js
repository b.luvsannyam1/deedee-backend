import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import FuseUtils from "@fuse/utils";

export const getBoundary = createAsyncThunk(
  "Boundary/boundary/getBoundary",
  async (params) => {
    console.log(params);
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/boundary/" + params
    );
    const data = await response.data.data;
    console.log(data);

    return data === undefined ? null : data;
  }
);

export const removeBoundary = createAsyncThunk(
  "Boundary/boundary/removeBoundary",
  async (val, { dispatch, getState }) => {
    const { _id } = getState().Boundary.boundary;
    await axios.delete("http://localhost:5000/server/v1/admin/boundary/" + _id);

    return _id;
  }
);

export const saveBoundary = createAsyncThunk(
  "Boundary/boundary/saveBoundary",
  async (boundaryData, { dispatch, getState }) => {
    const { boundary } = getState().Boundary;

    let data;
    console.log(boundary._id);
    if (boundary._id) {
      const response = await axios.put(
        "http://localhost:5000/server/v1/admin/boundary/" + boundary._id,
        {
          ...boundary,
          ...boundaryData,
        }
      );
      data = await response.data;
    } else {
      const response = await axios.post(
        "http://localhost:5000/server/v1/admin/boundary/",
        {
          ...boundary,
          ...boundaryData,
        }
      );
      data = await response.data[0];
    }
    return data;
  }
);

const boundarySlice = createSlice({
  name: "Boundary/boundary",
  initialState: null,
  reducers: {
    resetBoundary: () => null,
    newBoundary: {
      reducer: (state, action) => action.payload,
      prepare: (event) => ({
        payload: {
          _id: "",
          boundaryName: "",
          details: "",
        },
      }),
    },
  },
  extraReducers: {
    [getBoundary.fulfilled]: (state, action) => action.payload,
    [saveBoundary.fulfilled]: (state, action) => action.payload,
    [removeBoundary.fulfilled]: (state, action) => null,
  },
});

export const { newBoundary, resetBoundary } = boundarySlice.actions;

export default boundarySlice.reducer;

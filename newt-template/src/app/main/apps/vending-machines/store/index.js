import { combineReducers } from '@reduxjs/toolkit';
import order from './orderSlice';
import orders from './ordersSlice';
import product from './vendingSlice';
import products from './vendingsSlice';

const reducer = combineReducers({
	products,
	product,
	orders,
	order
});

export default reducer;

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import FuseUtils from '@fuse/utils';

export const getVending = createAsyncThunk('eCommerceApp/vending/getVending', async params => {
	const response = await axios.get('/api/e-commerce-app/vending', { params });
	const data = await response.data;

	return data === undefined ? null : data;
});

export const removeVending = createAsyncThunk(
	'eCommerceApp/vending/removeVending',
	async (val, { dispatch, getState }) => {
		const { id } = getState().eCommerceApp.vending;
		await axios.post('/api/e-commerce-app/remove-vending', { id });

		return id;
	}
);

export const saveVending = createAsyncThunk(
	'eCommerceApp/vending/saveVending',
	async (vendingData, { dispatch, getState }) => {
		const { vending } = getState().eCommerceApp;

		const response = await axios.post('/api/e-commerce-app/vending/save', { ...vending, ...vendingData });
		const data = await response.data;

		return data;
	}
);

const vendingSlice = createSlice({
	name: 'eCommerceApp/vending',
	initialState: null,
	reducers: {
		resetVending: () => null,
		newVending: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					id: FuseUtils.generateGUID(),
					name: '',
					handle: '',
					description: '',
					categories: [],
					tags: [],
					images: [],
					priceTaxExcl: 0,
					priceTaxIncl: 0,
					taxRate: 0,
					comparedPrice: 0,
					quantity: 0,
					sku: '',
					width: '',
					height: '',
					depth: '',
					weight: '',
					extraShippingFee: 0,
					active: true
				}
			})
		}
	},
	extraReducers: {
		[getVending.fulfilled]: (state, action) => action.payload,
		[saveVending.fulfilled]: (state, action) => action.payload,
		[removeVending.fulfilled]: (state, action) => null
	}
});

export const { newVending, resetVending } = vendingSlice.actions;

export default vendingSlice.reducer;

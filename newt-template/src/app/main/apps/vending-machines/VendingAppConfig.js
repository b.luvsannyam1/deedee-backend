import { lazy } from 'react';
import { Redirect } from 'react-router-dom';

const VendingAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/vending-machines/products/:productId/:productHandle?',
			component: lazy(() => import('./product/Product'))
		},
		{
			path: '/apps/vending-machines/products',
			component: lazy(() => import('./products/Products'))
		},
		{
			path: '/apps/vending-machines/orders/:orderId',
			component: lazy(() => import('./order/Order'))
		},
		{
			path: '/apps/vending-machines/orders',
			component: lazy(() => import('./orders/Orders'))
		},
		{
			path: '/apps/vending-machines',
			component: () => <Redirect to="/apps/vending-machines/products" />
		}
	]
};

export default VendingAppConfig;

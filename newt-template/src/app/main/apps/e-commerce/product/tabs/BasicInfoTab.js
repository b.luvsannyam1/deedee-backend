import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Switch from "@material-ui/core/Switch";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Button from "@material-ui/core/Button";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import "./Product.css";
import "./ckeditor.css";
import { useDispatch, useSelector } from "react-redux";
import { Card } from "@material-ui/core";
import Uploader from "./Uploader";
import { useFormContext, Controller } from "react-hook-form";
import { saveProduct, removeProduct } from "../../store/productSlice";

function BasicInfoTab(props) {
  const dispatch = useDispatch();
  const product = useSelector(({ eCommerceApp }) => eCommerceApp.product);
  console.log("product");
  console.log(product);
  const [Name, setName] = useState("");
  const [Description, setDescription] = useState("");
  const [Featured, setFeatured] = useState("");
  const [Image, setImage] = useState([]);
  const [Price, setPrice] = useState([]);
  const [Boundary, setBoundary] = useState([]);
  const [Brand, setBrand] = useState("");
  const [Filter, setFilter] = useState([]);
  const [Manufacturer, setManufacturer] = useState(false);
  const [Type, setType] = useState("");
  const [featureOn, setfeatureOn] = useState("");
  const [isActive, setActive] = useState(false);
  const [html, setHtml] = useState("");
  const [helpers, sethelpers] = useState(false);
  const prepareHelpers = (result) => {
    console.log(result);
    sethelpers(result);
  };
  useEffect(() => {
    const setFromData = () => {
      if (product.name !== "") {
        setPrice(product.price);
        setName(product.name);
        setDescription(product.description);
        setFeatured(product.featuredImageId);
        setImage(product.images);
        setBoundary(product.boundary);
        setBrand(product.brand);
        setFilter(product.filters);
        setManufacturer(product.manufacturer);
        setType(product.type);
        setfeatureOn(product.featureOn);
        setActive(product.activeForUsers);
      }
    };
    setFromData();
  }, [helpers]);
  useEffect(() => {
    const fetchHelpers = () => {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };

      fetch(
        "http://localhost:5000/server/v1/admin/product/help",
        requestOptions
      )
        .then((response) => response.json())
        .then((result) => prepareHelpers(result))
        .catch((error) => console.log("error", error));
    };
    if (!helpers) {
      fetchHelpers();
    }
  }, []);

  const ImageOnChange = async (e) => {
    console.log(e.target.files[0]);
    var formdata = new FormData();
    formdata.append("upload", e.target.files[0], e.target.files[0].name);

    var requestOptions = {
      method: "POST",
      body: formdata,
      redirect: "follow",
    };

    fetch("http://localhost:5000/server/v1/admin/product/image", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result.url);
        setImage([...Image, result.url]);
      })
      .catch((error) => console.log("error", error));
  };
  const DeleteImage = (image) => {
    const index = Image.indexOf(image);

    if (index > -1) {
      let deleted;
      deleted = Image.filter((item) => item !== image);
      console.log(deleted);
      setImage(deleted);
    }
  };
  const DeleteFilter = (image) => {
    const index = Filter.indexOf(image);

    if (index > -1) {
      let deleted;
      deleted = Filter.filter((item) => item !== image);
      console.log(deleted);
      setFilter(deleted);
    }
  };
  const DeleteBoundary = (image) => {
    const index = Boundary.indexOf(image);

    if (index > -1) {
      let deleted;
      deleted = Boundary.filter((item) => item !== image);
      console.log(deleted);
      setBoundary(deleted);
    }
  };
  function handleSaveProduct() {
    const values = {
      ...product,
      html: html,
      filters: Filter,
      boundary: Boundary,
      description: Description,
      images: Image,
      sales: 0,
      price: Price,
      name: Name,
      brand: Brand,
      manufacturer: Manufacturer,
      type: Type,
      featureOn: featureOn,
      featuredImageId: Featured,
      weight: "40",
      activeForUsers: isActive,
    };
    dispatch(saveProduct(values));
  }
  console.log(Filter, Boundary);
  return (
    <div>
      <TextField
        value={Name}
        onChange={(e) => {
          setName(e.target.value);
        }}
        className="mt-8 mb-16"
        required
        label="Гарчиг"
        autoFocus
        id="name"
        variant="outlined"
        fullWidth
      />{" "}
      <TextField
        value={Price}
        onChange={(e) => {
          setPrice(e.target.value);
        }}
        className="mt-8 mb-16"
        required
        label="Үнэ"
        autoFocus
        id="name"
        variant="outlined"
        fullWidth
      />
      <TextField
        value={Description}
        onChange={(e) => {
          setDescription(e.target.value);
        }}
        className="mt-8 mb-16"
        required
        label="Тайлбар"
        autoFocus
        id="description"
        variant="outlined"
        fullWidth
      />
      <div>
        <Typography className=" font-medium text-20">Зураг</Typography>

        <Typography className=" font-medium text-20">Онцлох зураг</Typography>
        {Featured !== "" ? (
          <img style={{ maxHeight: 300 }} src={Image[Featured]} />
        ) : (
          <div style={{ color: "orange" }}>
            Зураг онцлоогүй байна заавал онцолно уу
          </div>
        )}
        <Typography className=" font-medium text-20">Зургууд</Typography>
        <Card
          style={{
            display: "flex",
            flexWrap: "wrap",
          }}
        >
          {Image.map((item) => {
            return (
              <div style={{ position: "relative" }}>
                <img style={{ maxHeight: 300 }} src={item}></img>
                <div>
                  <button
                    onClick={() => {
                      setFeatured(Image.indexOf(item));
                    }}
                  >
                    Онцлох
                  </button>
                  <button
                    onClick={() => {
                      DeleteImage(item);
                    }}
                  >
                    Устгах
                  </button>
                </div>
              </div>
            );
          })}
          <input
            accept="image/*"
            type="file"
            id="button-file"
            label="Зураг"
            onChange={(e) =>
              ImageOnChange(e, (data) => {
                field.value = data;
              })
            }
          />
        </Card>
      </div>
      <div>
        <Typography className=" font-medium text-20">Шүүлтүүр</Typography>
        <Card style={{ display: "flex", flexWrap: "wrap" }}>
          {Filter !== []
            ? Filter.map((item) => {
                return (
                  <div
                    style={{
                      padding: 10,
                      borderRadius: 20,
                      backgroundColor: "orange",
                    }}
                    onClick={() => DeleteFilter(item)}
                  >
                    {item.name} X
                  </div>
                );
              })
            : ""}
        </Card>
        <div className="product-select">
          {/* <Typography className=" font-medium text-20">Шүүлтүүр</Typography> */}
          {helpers === false ? (
            ""
          ) : (
            <Select
              variant="outlined"
              onChange={(event) => {
                console.log(event);
                if (!Filter.includes(event.target.value))
                  setFilter([...Filter, event.target.value]);
              }}
            >
              {helpers.filter.map((item) => (
                <MenuItem value={item}>{item.name}</MenuItem>
              ))}
            </Select>
          )}
        </div>
      </div>
      <div>
        <Typography className=" font-medium text-20">Хязгаарлалтууд</Typography>
        <Card style={{ display: "flex", flexWrap: "wrap" }}>
          {Boundary !== []
            ? Boundary.map((item) => {
                return (
                  <div
                    style={{
                      padding: 10,
                      borderRadius: 20,
                      backgroundColor: "orange",
                    }}
                    onClick={() => DeleteBoundary(item)}
                  >
                    {item.boundaryName} X
                  </div>
                );
              })
            : ""}
        </Card>
        <div className="product-select">
          {/* <Typography className=" font-medium text-20">Хязгаарлалт</Typography> */}
          {helpers === false ? (
            ""
          ) : (
            <Select
              variant="outlined"
              onChange={(event) => {
                console.log(event);
                if (!Boundary.includes(event.target.value))
                  setBoundary([...Boundary, event.target.value]);
              }}
            >
              {helpers.boundary.map((item) => (
                <MenuItem value={item}>{item.boundaryName}</MenuItem>
              ))}
            </Select>
          )}
        </div>
      </div>
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        <div className="product-select">
          <Typography className=" font-medium text-20">Бренд</Typography>
          {helpers === false ? (
            ""
          ) : (
            <Select
              variant="outlined"
              value={Brand}
              onChange={(event) => {
                console.log(event);
                setBrand(event.target.value);
              }}
            >
              {helpers.brand.map((item) => (
                <MenuItem value={item._id}>{item.brandName}</MenuItem>
              ))}
            </Select>
          )}
        </div>
        <div className="product-select">
          <Typography className=" font-medium text-20">Үйлдвэрлэгч</Typography>
          {helpers === false ? (
            ""
          ) : (
            <Select
              variant="outlined"
              value={Manufacturer}
              onChange={(event) => {
                console.log(event);
                setManufacturer(event.target.value);
              }}
            >
              {helpers.manufacturer.map((item) => (
                <MenuItem value={item._id}>{item.manufacturerName}</MenuItem>
              ))}
            </Select>
          )}
        </div>
        <div className="product-select">
          <Typography className=" font-medium text-20">Төрөл</Typography>
          {helpers === false ? (
            ""
          ) : (
            <Select
              variant="outlined"
              value={Type}
              onChange={(event) => {
                console.log(event);
                setType(event.target.value);
              }}
            >
              {helpers.type.map((item) => (
                <MenuItem value={item._id}>{item.typeName}</MenuItem>
              ))}
            </Select>
          )}
        </div>

        <div className="product-select">
          <Typography className=" font-medium text-20">Хаана онцлох</Typography>
          <Select
            variant="outlined"
            value={featureOn}
            onChange={(event) => {
              console.log(event);
              setfeatureOn(event.target.value);
            }}
          >
            <MenuItem value="none">Үгүй</MenuItem>
            <MenuItem value="featured">Онцлох</MenuItem>
            <MenuItem value="performance">Performance дээр нэмэх</MenuItem>
            <MenuItem value="system">Онцлох систем дээр нэмэх</MenuItem>
          </Select>
        </div>

        <div className="product-select">
          <Typography className=" font-medium text-20">
            Хэрэглэгчидэд нээлттэй эсэх
          </Typography>
          <Switch
            checked={isActive}
            onChange={() => {
              setActive(!isActive);
            }}
          />
        </div>
      </div>{" "}
      <Typography className=" font-medium text-20">
        Дэлгэрэнгүй мэдээлэл
      </Typography>
      <CKEditor
        config={{ extraPlugins: [Uploader] }}
        editor={ClassicEditor}
        data={html}
        onChange={(event, editor) => setHtml(editor.getData())}
      />
      <div style={{ paddingTop: 30 }}>
        {" "}
        <Button
          className="whitespace-nowrap mx-4"
          variant="contained"
          color="secondary"
          // disabled={_.isEmpty(dirtyFields) || !isValid}
          onClick={handleSaveProduct}
        >
          Хадгалах
        </Button>
      </div>
    </div>
  );
}

export default BasicInfoTab;

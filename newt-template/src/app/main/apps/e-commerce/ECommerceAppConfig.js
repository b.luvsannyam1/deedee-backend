import { lazy } from "react";
import { Redirect } from "react-router-dom";

const ECommerceAppConfig = {
  settings: {
    layout: {},
  },
  routes: [
    {
      path: "/apps/e-commerce/products/:productId/:productHandle?",
      component: lazy(() => import("./product/Product")),
    },
    {
      path: "/apps/e-commerce/products",
      component: lazy(() => import("./products/Products")),
    },
  ],
};

export default ECommerceAppConfig;

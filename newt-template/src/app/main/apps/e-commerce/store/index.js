import { combineReducers } from "@reduxjs/toolkit";

import product from "./productSlice";
import products from "./productsSlice";
import order from "./orderSlice";
import orders from "./ordersSlice";

const reducer = combineReducers({
  order,
  orders,
  products,
  product,
});

export default reducer;

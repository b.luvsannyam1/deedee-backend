import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import FuseUtils from "@fuse/utils";

export const getProduct = createAsyncThunk(
  "eCommerceApp/product/getProduct",
  async (params) => {
    console.log(params);
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/product?_id=" + params.productId
    );
    const data = await response.data.data[0];
    console.log("one data");
    console.log(data);

    return data === undefined ? null : data;
  }
);

export const removeProduct = createAsyncThunk(
  "eCommerceApp/product/removeProduct",
  async (val, { dispatch, getState }) => {
    const { _id } = getState().eCommerceApp.product;
    await axios.delete("http://localhost:5000/server/v1/admin/product/" + _id);

    return _id;
  }
);

export const saveProduct = createAsyncThunk(
  "eCommerceApp/product/saveProduct",
  async (productData, { dispatch, getState }) => {
    const { product } = getState().eCommerceApp;

    let data;
    console.log(product._id);
    if (product._id) {
      const response = await axios.put(
        "http://localhost:5000/server/v1/admin/product/" + product._id,
        {
          ...product,
          ...productData,
        }
      );
      data = await response.data;
    } else {
      const response = await axios.post(
        "http://localhost:5000/server/v1/admin/product/",
        {
          ...product,
          ...productData,
        }
      );
      data = await response.data[0];
    }
  }
);

const productSlice = createSlice({
  name: "eCommerceApp/product",
  initialState: null,
  reducers: {
    resetProduct: () => null,
    newProduct: {
      reducer: (state, action) => action.payload,
      prepare: (event) => ({
        payload: {
          id: FuseUtils.generateGUID(),
          name: "",
          handle: "",
          description: "",
          categories: [],
          tags: [],
          images: [],
          priceTaxExcl: 0,
          priceTaxIncl: 0,
          taxRate: 0,
          comparedPrice: 0,
          quantity: 0,
          sku: "",
          width: "",
          height: "",
          depth: "",
          weight: "",
          extraShippingFee: 0,
          active: true,
          new: true,
        },
      }),
    },
  },
  extraReducers: {
    [getProduct.fulfilled]: (state, action) => action.payload,
    [saveProduct.fulfilled]: (state, action) => action.payload,
    [removeProduct.fulfilled]: (state, action) => null,
  },
});

export const { newProduct, resetProduct } = productSlice.actions;

export default productSlice.reducer;

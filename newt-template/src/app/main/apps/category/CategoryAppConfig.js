import { lazy } from "react";
import { Redirect } from "react-router-dom";

const CategoryAppConfig = {
  settings: {
    layout: {},
  },
  routes: [
    {
      path: "/apps/category/categories/:categoryId",
      component: lazy(() => import("./category/Category")),
    },
    {
      path: "/apps/category/categories",
      component: lazy(() => import("./categories/Categories")),
    },

    {
      path: "/apps/category",
      component: () => <Redirect to="/apps/category/categories" />,
    },
  ],
};

export default CategoryAppConfig;

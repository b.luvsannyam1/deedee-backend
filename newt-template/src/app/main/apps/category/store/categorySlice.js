import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import FuseUtils from "@fuse/utils";

export const getCategory = createAsyncThunk(
  "Category/category/getCategory",
  async (params) => {
    console.log(params);
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/productcategory/" + params
    );
    const data = await response.data.data;
    console.log(data);

    return data === undefined ? null : data;
  }
);

export const removeCategory = createAsyncThunk(
  "Category/category/removeCategory",
  async (val, { dispatch, getState }) => {
    const { _id } = getState().Category.category;
    await axios.delete(
      "http://localhost:5000/server/v1/admin/productcategory/" + _id
    );

    return _id;
  }
);

export const saveCategory = createAsyncThunk(
  "Category/category/saveCategory",
  async (categoryData, { dispatch, getState }) => {
    const { category } = getState().Category;
    let data;
    console.log(category._id);
    if (category._id) {
      const response = await axios.put(
        "http://localhost:5000/server/v1/admin/productcategory/" + category._id,
        {
          ...category,
          ...categoryData,
        }
      );
      data = await response.data;
    } else {
      const response = await axios.post(
        "http://localhost:5000/server/v1/admin/productcategory/",
        {
          ...category,
          ...categoryData,
        }
      );
      data = await response.data[0];
    }
    return data;
  }
);

const categorySlice = createSlice({
  name: "Category/category",
  initialState: null,
  reducers: {
    resetCategory: () => null,
    newCategory: {
      reducer: (state, action) => action.payload,
      prepare: (event) => ({
        payload: {
          id: FuseUtils.generateGUID(),
          name: "",
          handle: "",
          description: "",
          categories: [],
          tags: [],
          images: [],
          priceTaxExcl: 0,
          priceTaxIncl: 0,
          taxRate: 0,
          comparedPrice: 0,
          quantity: 0,
          sku: "",
          width: "",
          height: "",
          depth: "",
          weight: "",
          extraShippingFee: 0,
          active: true,
        },
      }),
    },
  },
  extraReducers: {
    [getCategory.fulfilled]: (state, action) => action.payload,
    [saveCategory.fulfilled]: (state, action) => action.payload,
    [removeCategory.fulfilled]: (state, action) => null,
  },
});

export const { newCategory, resetCategory } = categorySlice.actions;

export default categorySlice.reducer;

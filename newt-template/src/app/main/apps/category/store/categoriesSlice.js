import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getCategories = createAsyncThunk(
  "Categories/categories/getCategories",
  async () => {
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/productcategory/"
    );
    const data = await response.data.data;
    console.log(data);
    return data;
  }
);

export const removeCategories = createAsyncThunk(
  "Categories/categories/removeCategories",
  async (productIds, { dispatch, getState }) => {
    await axios.post("/api/e-commerce-app/remove-categories", { productIds });

    return productIds;
  }
);

const categoriesAdapter = createEntityAdapter({
  // Assume IDs are stored in a field other than `book.id`
  selectId: (book) => book._id,
  // Keep the "all IDs" array sorted based on book titles
  sortComparer: (a, b) => a.name.localeCompare(b.name),
});

export const { selectAll: selectCategories, selectById: selectProductById } =
  categoriesAdapter.getSelectors((state) => state.Categories.categories);

const categoriesSlice = createSlice({
  name: "Categories/categories",
  initialState: categoriesAdapter.getInitialState({
    searchText: "",
  }),
  reducers: {
    setCategoriesSearchText: {
      reducer: (state, action) => {
        state.searchText = action.payload;
      },
      prepare: (event) => ({ payload: event.target.value || "" }),
    },
  },
  extraReducers: {
    [getCategories.fulfilled]: categoriesAdapter.setAll,
    [removeCategories.fulfilled]: (state, action) =>
      categoriesAdapter.removeMany(state, action.payload),
  },
});

export const { setCategoriesSearchText } = categoriesSlice.actions;

export default categoriesSlice.reducer;

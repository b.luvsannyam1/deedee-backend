import AcademyAppConfig from "./academy/AcademyAppConfig";
import CalendarAppConfig from "./calendar/CalendarAppConfig";
import ChatAppConfig from "./chat/ChatAppConfig";
import ContactsAppConfig from "./contacts/ContactsAppConfig";
import AnalyticsDashboardAppConfig from "./dashboards/analytics/AnalyticsDashboardAppConfig";
import ProjectDashboardAppConfig from "./dashboards/project/ProjectDashboardAppConfig";
import ECommerceAppConfig from "./e-commerce/ECommerceAppConfig";
import FileManagerAppConfig from "./file-manager/FileManagerAppConfig";
import MailAppConfig from "./mail/MailAppConfig";
import NotesAppConfig from "./notes/NotesAppConfig";
import ScrumboardAppConfig from "./scrumboard/ScrumboardAppConfig";
import TodoAppConfig from "./todo/TodoAppConfig";
import BrandsAppConfig from "./brands/BrandsAppConfig";
import ManufacturersAppConfig from "./manufacturers/ManufacturersAppConfig";
import FiltersAppConfig from "./filters/FiltersAppConfig";
import TypeAppConfig from "./type/TypeAppConfig";
import BoundaryAppConfig from "./boundary/BoundaryAppConfig";
import CategoryAppConfig from "./category/CategoryAppConfig";
import CardAppConfig from "./cards/CardAppConfig";
import BlogAppConfig from "./blog/BlogAppConfig";

const appsConfigs = [
  AnalyticsDashboardAppConfig,
  ProjectDashboardAppConfig,
  MailAppConfig,
  TodoAppConfig,
  FileManagerAppConfig,
  ContactsAppConfig,
  CalendarAppConfig,
  ChatAppConfig,
  ECommerceAppConfig,
  ScrumboardAppConfig,
  AcademyAppConfig,
  NotesAppConfig,
  // VendingAppConfig,
  BrandsAppConfig,
  ManufacturersAppConfig,
  FiltersAppConfig,
  TypeAppConfig,
  BoundaryAppConfig,
  CategoryAppConfig,
  CardAppConfig,
  BlogAppConfig,
];

export default appsConfigs;

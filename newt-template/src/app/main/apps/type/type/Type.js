import FuseLoading from "@fuse/core/FuseLoading";
import FusePageCarded from "@fuse/core/FusePageCarded";
import { useDeepCompareEffect } from "@fuse/hooks";
import Button from "@material-ui/core/Button";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import withReducer from "app/store/withReducer";
import { motion } from "framer-motion";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import _ from "@lodash";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { resetType, newType, getType } from "../store/typeSlice";
import reducer from "../store";
import TypeHeader from "./TypeHeader";
import BasicInfoTab from "./tabs/BasicInfoTab";

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
  name: yup
    .string()
    .required("You must enter a type name")
    .min(5, "The type name must be at least 5 characters"),
});

function Type(props) {
  const dispatch = useDispatch();
  const type = useSelector(({ Type }) => Type.type);

  const routeParams = useParams();
  const [tabValue, setTabValue] = useState(0);
  const [noType, setNoType] = useState(false);
  const methods = useForm({
    mode: "onChange",
    defaultValues: {},
    resolver: yupResolver(schema),
  });
  const { reset, watch, control, onChange, formState } = methods;
  const form = watch();

  useDeepCompareEffect(() => {
    function updateTypeState() {
      const { typeId } = routeParams;

      if (typeId === "new") {
        /**
         * Create New Type data
         */
        dispatch(newType());
      } else {
        /**
         * Get Type data
         */
        dispatch(getType(typeId)).then((action) => {
          /**
           * If the requested type is not exist show message
           */
          if (!action.payload) {
            setNoType(true);
          }
        });
      }
    }

    updateTypeState();
  }, [dispatch, routeParams]);

  useEffect(() => {
    if (!type) {
      return;
    }
    /**
     * Reset the form on type state changes
     */
    reset(type);
  }, [type, reset]);

  useEffect(() => {
    return () => {
      /**
       * Reset Type on component unload
       */
      dispatch(resetType());
      setNoType(false);
    };
  }, [dispatch]);

  /**
   * Tab Change
   */
  function handleTabChange(event, value) {
    setTabValue(value);
  }

  /**
   * Show Message if the requested types is not exists
   */
  if (noType) {
    return (
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1, transition: { delay: 0.1 } }}
        className="flex flex-col flex-1 items-center justify-center h-full"
      >
        <Typography color="textSecondary" variant="h5">
          There is no such type!
        </Typography>
        <Button
          className="mt-24"
          component={Link}
          variant="outlined"
          to="/apps/type/types"
          color="inherit"
        >
          Go to Types Page
        </Button>
      </motion.div>
    );
  }

  /**
   * Wait while type data is loading and form is setted
   */
  if (
    _.isEmpty(form) ||
    (type && routeParams.typeId !== type._id && routeParams.typeId !== "new")
  ) {
    return <FuseLoading />;
  }

  return (
    <FormProvider {...methods}>
      <FusePageCarded
        classes={{
          toolbar: "p-0",
          header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
        }}
        header={<TypeHeader />}
        contentToolbar={
          <Tabs
            value={tabValue}
            onChange={handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            classes={{ root: "w-full h-64" }}
          >
            <Tab className="h-64" label="Basic Info" />
          </Tabs>
        }
        content={
          <div className="p-16 sm:p-24 max-w-2xl">
            <div className={tabValue !== 0 ? "hidden" : ""}>
              <BasicInfoTab />
            </div>
          </div>
        }
        innerScroll
      />
    </FormProvider>
  );
}

export default withReducer("Type", reducer)(Type);

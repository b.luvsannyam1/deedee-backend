import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import "./ckeditor.css";
import { useFormContext, Controller } from "react-hook-form";

function BasicInfoTab(props) {
  const methods = useFormContext();
  const { control, formState } = methods;
  const { errors } = formState;
  return (
    <div>
      <Controller
        name="typeName"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            className="mt-8 mb-16"
            error={!!errors.typeName}
            required
            helperText={errors?.typeName?.message}
            label="Name"
            autoFocus
            id="brandName"
            variant="outlined"
            fullWidth
          />
        )}
      />
      <Controller
        name="details"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            className="mt-8 mb-16"
            id="Details"
            label="details"
            type="text"
            multiline
            rows={5}
            variant="outlined"
            fullWidth
          />
        )}
      />
    </div>
  );
}

export default BasicInfoTab;

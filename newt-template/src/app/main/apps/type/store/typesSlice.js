import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getTypes = createAsyncThunk("Types/types/getTypes", async () => {
  const response = await axios.get(
    "http://localhost:5000/server/v1/admin/type/"
  );
  const data = await response.data.data;
  console.log(data);
  return data;
});

export const removeTypes = createAsyncThunk(
  "Types/types/removeTypes",
  async (productIds, { dispatch, getState }) => {
    await axios.post("/api/e-commerce-app/remove-types", { productIds });

    return productIds;
  }
);

const typesAdapter = createEntityAdapter({
  // Assume IDs are stored in a field other than `book.id`
  selectId: (book) => book._id,
  // Keep the "all IDs" array sorted based on book titles
  sortComparer: (a, b) => a.typeName.localeCompare(b.typeName),
});

export const { selectAll: selectTypes, selectById: selectTypeById } =
  typesAdapter.getSelectors((state) => state.Types.types);

const typesSlice = createSlice({
  name: "Types/types",
  initialState: typesAdapter.getInitialState({
    searchText: "",
  }),
  reducers: {
    setTypesSearchText: {
      reducer: (state, action) => {
        state.searchText = action.payload;
      },
      prepare: (event) => ({ payload: event.target.value || "" }),
    },
  },
  extraReducers: {
    [getTypes.fulfilled]: typesAdapter.setAll,
    [removeTypes.fulfilled]: (state, action) =>
      typesAdapter.removeMany(state, action.payload),
  },
});

export const { setTypesSearchText } = typesSlice.actions;

export default typesSlice.reducer;

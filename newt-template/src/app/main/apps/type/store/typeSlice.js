import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import FuseUtils from "@fuse/utils";

export const getType = createAsyncThunk(
  "Types/type/getType",
  async (params) => {
    console.log(params);
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/type/" + params
    );
    const data = await response.data.data;
    console.log(data);

    return data === undefined ? null : data;
  }
);

export const removeType = createAsyncThunk(
  "Types/type/removeType",
  async (val, { dispatch, getState }) => {
    const { _id } = getState().Type.type;
    await axios.delete("http://localhost:5000/server/v1/admin/type/" + _id);

    return _id;
  }
);

export const saveType = createAsyncThunk(
  "Types/type/saveType",
  async (typeData, { dispatch, getState }) => {
    const { type } = getState().Type;

    let data;
    console.log(type._id);
    if (type._id) {
      const response = await axios.put(
        "http://localhost:5000/server/v1/admin/type/" + type._id,
        {
          ...type,
          ...typeData,
        }
      );
      data = await response.data;
    } else {
      const response = await axios.post(
        "http://localhost:5000/server/v1/admin/type/",
        {
          ...type,
          ...typeData,
        }
      );
      data = await response.data[0];
    }
    return data;
  }
);

const typeSlice = createSlice({
  name: "Types/type",
  initialState: null,
  reducers: {
    resetType: () => null,
    newType: {
      reducer: (state, action) => action.payload,
      prepare: (event) => ({
        payload: {
          img_src: "/images/thumbnail/images.jpeg",
          typeName: "Test",
          details: "Test",
          inBuild: "build",
          html: "test",
        },
      }),
    },
  },
  extraReducers: {
    [getType.fulfilled]: (state, action) => action.payload,
    [saveType.fulfilled]: (state, action) => action.payload,
    [removeType.fulfilled]: (state, action) => null,
  },
});

export const { newType, resetType } = typeSlice.actions;

export default typeSlice.reducer;

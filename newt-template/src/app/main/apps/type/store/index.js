import { combineReducers } from "@reduxjs/toolkit";

import type from "./typeSlice";
import types from "./typesSlice";

const reducer = combineReducers({
  types,
  type,
});

export default reducer;

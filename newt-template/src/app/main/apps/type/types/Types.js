import FusePageCarded from "@fuse/core/FusePageCarded";
import withReducer from "app/store/withReducer";
import reducer from "../store";
import TypesHeader from "./TypesHeader";
import TypesTable from "./TypesTable";

function Types() {
  return (
    <FusePageCarded
      classes={{
        content: "flex",
        contentCard: "overflow-hidden",
        header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
      }}
      header={<TypesHeader />}
      content={<TypesTable />}
      innerScroll
    />
  );
}

export default withReducer("Types", reducer)(Types);

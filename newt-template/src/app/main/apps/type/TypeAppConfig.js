import { lazy } from "react";
import { Redirect } from "react-router-dom";

const TypeAppConfig = {
  settings: {
    layout: {},
  },
  routes: [
    {
      path: "/apps/types/types/:typeId",
      component: lazy(() => import("./type/Type")),
    },
    {
      path: "/apps/types/types",
      component: lazy(() => import("./types/Types")),
    },
    {
      path: "/apps/types",
      component: () => <Redirect to="/apps/type/types" />,
    },
  ],
};

export default TypeAppConfig;

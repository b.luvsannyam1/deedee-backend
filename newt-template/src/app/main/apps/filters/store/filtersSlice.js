import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getFilters = createAsyncThunk(
  "Filters/filters/getFilters",
  async () => {
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/filter/"
    );
    const data = await response.data.data;
    console.log(data);
    return data;
  }
);

export const removeFilters = createAsyncThunk(
  "Filters/filters/removeFilters",
  async (filterIds, { dispatch, getState }) => {
    await axios.post("/api/e-commerce-app/remove-filters", { filterIds });

    return filterIds;
  }
);

const filtersAdapter = createEntityAdapter({
  // Assume IDs are stored in a field other than `book.id`
  selectId: (book) => book._id,
  // Keep the "all IDs" array sorted based on book titles
  sortComparer: (a, b) => a.name.localeCompare(b.name),
});

export const { selectAll: selectFilters, selectById: selectFilterById } =
  filtersAdapter.getSelectors((state) => state.Filters.filters);

const filtersSlice = createSlice({
  name: "Filters/filters",
  initialState: filtersAdapter.getInitialState({
    searchText: "",
  }),
  reducers: {
    setFiltersSearchText: {
      reducer: (state, action) => {
        state.searchText = action.payload;
      },
      prepare: (event) => ({ payload: event.target.value || "" }),
    },
  },
  extraReducers: {
    [getFilters.fulfilled]: filtersAdapter.setAll,
    [removeFilters.fulfilled]: (state, action) =>
      filtersAdapter.removeMany(state, action.payload),
  },
});

export const { setFiltersSearchText } = filtersSlice.actions;

export default filtersSlice.reducer;

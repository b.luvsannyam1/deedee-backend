import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import FuseUtils from "@fuse/utils";

export const getFilter = createAsyncThunk(
  "Filters/filter/getFilter",
  async (params) => {
    console.log(params);
    const response = await axios.get(
      "http://localhost:5000/server/v1/admin/filter/" + params
    );
    const data = await response.data.data;
    console.log(data);

    return data === undefined ? null : data;
  }
);

export const removeFilter = createAsyncThunk(
  "Filters/filter/removeFilter",
  async (val, { dispatch, getState }) => {
    const { _id } = getState().Filter.filter;
    await axios.delete("http://localhost:5000/server/v1/admin/filter/" + _id);

    return _id;
  }
);

export const saveFilter = createAsyncThunk(
  "Filters/filter/saveFilter",
  async (filterData, { dispatch, getState }) => {
    const { filter } = getState().Filters;
    let data;
    console.log(filter._id);
    if (filter._id) {
      const response = await axios.put(
        "http://localhost:5000/server/v1/admin/filter/" + filter._id,
        {
          ...filter,
          ...filterData,
        }
      );
      data = await response.data;
    } else {
      const response = await axios.post(
        "http://localhost:5000/server/v1/admin/filter/",
        {
          ...filter,
          ...filterData,
        }
      );
      data = await response.data[0];
    }
    return data;
  }
);

const filterSlice = createSlice({
  name: "Filters/filter",
  initialState: null,
  reducers: {
    resetFilter: () => null,
    newFilter: {
      reducer: (state, action) => action.payload,
      prepare: (event) => ({
        payload: {
          name: "",
          description: "",
        },
      }),
    },
  },
  extraReducers: {
    [getFilter.fulfilled]: (state, action) => action.payload,
    [saveFilter.fulfilled]: (state, action) => action.payload,
    [removeFilter.fulfilled]: (state, action) => null,
  },
});

export const { newFilter, resetFilter } = filterSlice.actions;

export default filterSlice.reducer;

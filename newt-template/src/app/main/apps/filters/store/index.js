import { combineReducers } from "@reduxjs/toolkit";
import filter from "./filterSlice";
import filters from "./filtersSlice";

const reducer = combineReducers({
  filter,
  filters,
});

export default reducer;

import { lazy } from "react";
import { Redirect } from "react-router-dom";

const FiltersAppConfig = {
  settings: {
    layout: {},
  },
  routes: [
    {
      path: "/apps/filters/filters/:filterId",
      component: lazy(() => import("./filter/Filter")),
    },
    {
      path: "/apps/filters/filters",
      component: lazy(() => import("./filters/Filters")),
    },

    {
      path: "/apps/filters",
      component: () => <Redirect to="/apps/filters/filters" />,
    },
  ],
};

export default FiltersAppConfig;

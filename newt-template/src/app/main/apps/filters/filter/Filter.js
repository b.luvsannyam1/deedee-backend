import FuseLoading from "@fuse/core/FuseLoading";
import FusePageCarded from "@fuse/core/FusePageCarded";
import { useDeepCompareEffect } from "@fuse/hooks";
import Button from "@material-ui/core/Button";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import withReducer from "app/store/withReducer";
import { motion } from "framer-motion";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import _ from "@lodash";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { resetFilter, newFilter, getFilter } from "../store/filterSlice";
import reducer from "../store";
import FilterHeader from "./FilterHeader";
import BasicInfoTab from "./tabs/BasicInfoTab";
import InventoryTab from "./tabs/InventoryTab";
import PricingTab from "./tabs/PricingTab";
import FilterImagesTab from "./tabs/FilterImagesTab";
import ShippingTab from "./tabs/ShippingTab";

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
  name: yup
    .string()
    .required("You must enter a filter name")
    .min(5, "The filter name must be at least 5 characters"),
});

function Filter(props) {
  const dispatch = useDispatch();
  const filter = useSelector(({ Filter }) => Filter.filter);

  const routeParams = useParams();
  const [tabValue, setTabValue] = useState(0);
  const [noFilter, setNoFilter] = useState(false);
  const methods = useForm({
    mode: "onChange",
    defaultValues: {},
    resolver: yupResolver(schema),
  });
  const { reset, watch, control, onChange, formState } = methods;
  const form = watch();

  useDeepCompareEffect(() => {
    function updateFilterState() {
      const { filterId } = routeParams;

      if (filterId === "new") {
        /**
         * Create New Filter data
         */
        dispatch(newFilter());
      } else {
        /**
         * Get Filter data
         */
        dispatch(getFilter(filterId)).then((action) => {
          /**
           * If the requested filter is not exist show message
           */
          if (!action.payload) {
            setNoFilter(true);
          }
        });
      }
    }

    updateFilterState();
  }, [dispatch, routeParams]);

  useEffect(() => {
    if (!filter) {
      return;
    }
    /**
     * Reset the form on filter state changes
     */
    reset(filter);
  }, [filter, reset]);

  useEffect(() => {
    return () => {
      /**
       * Reset Filter on component unload
       */
      dispatch(resetFilter());
      setNoFilter(false);
    };
  }, [dispatch]);

  /**
   * Tab Change
   */
  function handleTabChange(event, value) {
    setTabValue(value);
  }

  /**
   * Show Message if the requested filters is not exists
   */
  if (noFilter) {
    return (
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1, transition: { delay: 0.1 } }}
        className="flex flex-col flex-1 items-center justify-center h-full"
      >
        <Typography color="textSecondary" variant="h5">
          There is no such filter!
        </Typography>
        <Button
          className="mt-24"
          component={Link}
          variant="outlined"
          to="/apps/filters/filters"
          color="inherit"
        >
          Go to Filter Page
        </Button>
      </motion.div>
    );
  }

  /**
   * Wait while filter data is loading and form is setted
   */
  if (
    _.isEmpty(form) ||
    (filter &&
      routeParams.filterId !== filter._id &&
      routeParams.filterId !== "new")
  ) {
    return <FuseLoading />;
  }

  return (
    <FormProvider {...methods}>
      <FusePageCarded
        classes={{
          toolbar: "p-0",
          header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
        }}
        header={<FilterHeader />}
        contentToolbar={
          <Tabs
            value={tabValue}
            onChange={handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            classes={{ root: "w-full h-64" }}
          >
            <Tab className="h-64" label="Basic Info" />w
          </Tabs>
        }
        content={
          <div className="p-16 sm:p-24 max-w-2xl">
            <div className={tabValue !== 0 ? "hidden" : ""}>
              <BasicInfoTab />
            </div>
          </div>
        }
        innerScroll
      />
    </FormProvider>
  );
}

export default withReducer("Filter", reducer)(Filter);
